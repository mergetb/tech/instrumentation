package helpers;

import (
    "os"
    "fmt"
    "google.golang.org/protobuf/proto"
    "google.golang.org/protobuf/encoding/protojson"

    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
)

func BuildDir(loc string) error {
    if err := os.MkdirAll(loc, os.ModePerm); err != nil {
        return err
    }

    // Check if the directory was created or already exists
    if _, err := os.Stat(loc); os.IsNotExist(err) {
        return err
    }

    return nil
}

func SaveToIntermediate(msg proto.Message, service string) error {
        if !config.Settings.LogToIntermediateFile { return nil }

	// Serialize the protobuf message to JSON
	data, err := protojson.Marshal(msg)
	if err != nil {
		return err
	}

        filePath := fmt.Sprintf("%v/%v-data.txt", config.Settings.IntermediateDataFolder, service)

	// Open the file in append mode, create it if it doesn't exist
	file, err := os.OpenFile(filePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
	    return err
	}
	defer file.Close()

	// Write the JSON data followed by a newline
	_, err = file.Write(append(data, '\n'))
	return err
}

