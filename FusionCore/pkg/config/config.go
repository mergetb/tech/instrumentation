package config;

import (
    "fmt"
    "io/ioutil"
    "gopkg.in/yaml.v2"
)

var Settings YamlConfig = NewYamlConfig();

type YamlConfig struct {
    // Logging information
    LogFile                        string `default:"/var/log/discern.log"`
    // For gRPC connection
    GRPC_Port                      int64  `default:50051`
    ListenIP                       string `default:"localhost"`
    ListenProto                    string `default:"tcp"`
    // For influxDB connection
    InfluxPort                     int64  `default:8086`
    InfluxUrl                      string `default:"http://localhost"`
    InfluxToken                    string `default:"BIGElHSa291FOkrliGaBVc7ksnGgQ4vALbkfJzRuH02T2XB8qouH0H3IkYTJACE-XZ-QYV664CH5655LkbQDIQ::"`
    StartNewInfluxInstance         bool   `default:false`
    InfluxDataMount                string `default:discerninflux`
    BUCKET_NAME                    string `default:"DISCERN"`
    ORG                            string `default:"ISI"`
    InfluxPassword                 string `default:"something"`
    InfluxUser                     string `default:"default-user"`
    // For postgres connection
    PostgresPort                   int64  `default:5432`
    PostgresDataMount              string `default:discernpsql`
    StartNewPostgresInstance       bool   `default: false`
    PostgresUser                   string `default: "postgres"`
    PostgresPassword               string `default: "*Something*"`
    PostgresIP                     string `default: "127.0.0.1"`
}


func NewYamlConfig() YamlConfig {
    return YamlConfig{
        LogFile                    : "/var/log/discern.log",
        GRPC_Port                  : 50051,
        ListenIP                   : "localhost",
        ListenProto                : "tcp",
        InfluxPort                 : 8086,
        InfluxUrl                  : "http://localhost",
        InfluxToken                : "BIGElHSa291FOkrliGaBVc7ksnGgQ4vALbkfJzRuH02T2XB8qouH0H3IkYTJACE-XZ-QYV664CH5655LkbQDIQ::",
        StartNewInfluxInstance     : false,
        InfluxDataMount            : "discerninflux",
        BUCKET_NAME                : "DISCERN",
        ORG                        : "ISI",
        InfluxPassword             : "something",
        InfluxUser                 : "default-user",
        PostgresPort               : 5432,
        PostgresDataMount          : "discernpsql",
        StartNewPostgresInstance   : false,
        PostgresUser               : "postgres",
        PostgresPassword           : "*Something*",
        PostgresIP                 : "127.0.0.1",
    }
}

func LoadConfig() error {

    yamlFile, err := ioutil.ReadFile("/etc/discern/CoreConfig.yaml")
    if err != nil {
        return fmt.Errorf("error reading config file: %v", err)
    }

    err = yaml.Unmarshal(yamlFile, &Settings)
    if err != nil {
        return fmt.Errorf("unmarshal error on config file: %v", err)
    }

    return nil
}

