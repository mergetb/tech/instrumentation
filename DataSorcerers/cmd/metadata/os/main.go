package main;

import (
    "fmt"
    "time"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"

    "gitlab.com/instrumentation/DataSorcerers/cmd/metadata/os/save"
);

var syscalls = []string{
    "close",
    "closerange",
    "execve",
    "execveat",
    "sysinfo",
    "fork",
    "vfork",
    "kill",
    "tkill",
    "open",
    "openat",
    "openat2",
    "recvfrom",
    "recvmmsg",
    "recvmsg",
    "sendmmsg",
    "sendmsg",
    "sendto",
    "socket",
    "socketpair",
}


func main() {

    config.LoadConfig()
    if !config.Settings.RunOs { return }

    err := log.InitializeLogOutput("OS")
    if err != nil {
        fmt.Printf("error ", err)
        return
    }

    // Connect to Fusion Core
    Saver := &save.OsFileSaver{}
    Saver.ConnectClient()

    // Make tmp storage folder
    err = helpers.BuildDir(config.Settings.OsDataDir)
    if err != nil {
        log.Fatalf("error building data directory: %v", err)
    }

    // Make folder to store final json data
    if config.Settings.LogToIntermediateFile {
        err = helpers.BuildDir(config.Settings.IntermediateDataFolder)
        if err != nil {
            log.Fatalf("error building intermediate data dir: %v", err)
        }
    }

    // The sensors timestamp their own data so we send when convenient
    for {
        for _, call := range syscalls {
            err = Saver.SaveData(call)
            if err != nil { // Note it, but don't let it stop the recording process
                log.Errorf("error saving %v syscall data: %v", call, err)
            }
        }

        log.Info("finished save data routine")

        time.Sleep(time.Duration(config.Settings.OsInfoDumpInterval) * time.Second)
    }
}

