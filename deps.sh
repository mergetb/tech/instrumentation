#!/bin/bash

set -ex

glibc_version="2.31"

# Determine what to install dependencies for
ALL=false
CORE=false
SORC=false
BUILD=false


# Check if sudo exists. If not, you're root
if command -v sudo > /dev/null 2>&1; then
    sudo=sudo
else
    sudo=""
fi


while [ $# -gt 0 ]; do
    case $1 in 
        all)
            ALL=true;
            shift;
            ;;
        core)
            CORE=true;
            shift;
            ;;
        sorcerers)
            SORC=true;
            shift;
            ;;
        build)
            BUILD=true;
            shift
            ;;
        analysis)
            ANALYZE=true;
            shift
            ;;
        *)
            echo "Unrecognized cli: $1. Exiting"
            exit 1
    esac
done

if [ "$ALL" = "false" ] && [ "$CORE" = "false" ] && [ "$SORC" = "false" ] && [ "$BUILD" = "false" ]; then
    ALL=true
fi


# Figure out what installer they are using
declare -l installer;
if [ -f /etc/redhat-release ]; then
    installer="yum"
    echo "$installer isn't a supported installer. Please add support or switch OS"
    exit 1
elif [ -f /etc/arch-release ]; then
     installer="pacman"
    echo "$installer isn't a supported installer. Please add support or switch OS"
    exit 1
elif [ -f /etc/gentoo-release ]; then
     installer="emerge"
    echo "$installer isn't a supported installer. Please add support or switch OS"
    exit 1
elif [ -f /etc/SuSE-release ]; then
    installer="zypp"
    echo "$installer isn't a supported installer. Please add support or switch OS"
    exit 1
elif [ -f /etc/debian_version ] || [ -f /etc/ubuntu_version ]; then
     installer="apt"
     $sudo apt update -y && $sudo apt upgrade -y
elif [ -f /etc/alpine-release ]; then
     installer="apk"
    echo "$installer isn't a supported installer. Please add support or switch OS"
    exit 1
fi


# If user wants to install the build dependencies
if [ "$BUILD" = true ] || [ "$ALL" = true ]; then
    echo "Installing build dependencies"
    # If you are building anything, you need the bridge. We install that here
    if [ "$installer" = "apt" ]; then
        $sudo apt install -y wget
        $sudo apt install -y make
        $sudo apt install -y protobuf-compiler

        if command -v go >/dev/null 2>&1; then
            echo "Go is installed. Using local installation"
        else
            wget  https://go.dev/dl/go1.23.2.linux-amd64.tar.gz
            $sudo tar -C /usr/local -xzf go1.23.2.linux-amd64.tar.gz
            export PATH="$PATH:/usr/local/go/bin"
            export PATH="$PATH:$(go env GOPATH)/bin"
            echo "export PATH=$PATH:/usr/local/go/bin:$(go env GOPATH)/bin" >> /home/$SUDO_USER/.profile || true
            echo "export PATH=$PATH:/usr/local/go/bin:$(go env GOPATH)/bin" >> /home/$(whoami)/.profile || true
        fi

        if command -v gcc >/dev/null 2>&1; then
            echo "gcc is installed. Using local installation"
        else
            $sudo apt install -y gcc
        fi
    fi

    # These are the build options for the sorcerers
    if [ "$installer" = "apt" ]; then
        $sudo apt install -y libpcap-dev 
        $sudo apt install -y gawk bison

        current_glic_verison=$(ldd --version | head -1 | awk '{print $NF}')

        # Verify the version of glibc that we are linking against
        if [ "$current_glic_verison" != "$glibc_version" ]; then
            pushd .
                mkdir -p DataSorcerers/glibc
                mkdir -p DataSorcerers/glibc/build
                mkdir "DataSorcerers/glibc/glibc-$glibc_version-install"

                cd DataSorcerers/glibc 

                wget http://ftp.gnu.org/gnu/libc/glibc-2.31.tar.gz
                tar -xvzf glibc-2.31.tar.gz

                cd build
                ../glibc-2.31/configure --prefix=../glibc-2.31-install --disable-werror
                make
                make install
            popd
        fi
    fi

    # These are the build deps for the FusionCore
    if [ "$installer" = "apt" ]; then
        $sudo apt install -y docker.io
        $sudo usermod -aG docker $(whoami)
        $sudo chmod 666 /var/run/docker.sock || true
    fi

    # Install go dependencies
    go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
    go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
fi


# Install the core run-time dependencies
if [ "$ALL" = "true" ] || [ "$CORE" = "true" ]; then
    echo "Installing FusionCore run-time dependencies"
    if [ "$installer" = "apt" ]; then
        $sudo apt install -y docker.io
        $sudo usermod -aG docker $(whoami)
        $sudo chmod 666 /var/run/docker.sock || true
    fi
fi


# Install the Sorcerer run-time dependencies (there are none)
if [ "$ALL" = "true" ] || [ "$SORC" = "true" ]; then
    echo "Installing DataSorcerers run-time dependencies"
    if [ "$installer" = "apt" ]; then
        $sudo apt install -y git
        $sudo apt install -y bpftrace
        $sudo apt -y install perl
        $sudo apt -y install git
        $sudo apt -y install strace
        $sudo apt install -y linux-headers-$(uname -r)
    fi
fi


if [ "$ANALYZE" = "true" ]; then
    if command -v python3 > /dev/null 2>&1; then
        echo "Using local installation of python3"
    else
        if [ "$installer" = "apt" ]; then
            $sudo apt install -y python3
        else 
            echo "Package manager $installer not currently supported. Feel free to add support"
            echo "Not installing analysis dependencies. Exiting"
            exit 1
        fi
    fi

    if command -v pip > /dev/null 2>&1; then
        echo "Using local install of pip"
    else
        if [ "$installer" = "apt" ]; then
            $sudo apt install -y python3-pip
        else 
            echo "Package manager $installer not currently supported. Feel free to add support"
            echo "We are using pip to install the dependencies. Please check if your OS allows python to manage its own packages"
            echo "Not installing analysis dependencies. Exiting"
            exit 1
        fi
    fi

    python3 -m pip install influxdb
    python3 -m pip install influxdb-client
fi

echo "Sucessfully installed dependencies!"

