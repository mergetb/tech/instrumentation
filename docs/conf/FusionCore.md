# The FusionCore's Configuration Options

This document is going to dry-ly list all the different configuration options you can specify. If 
you have architectural questions, please see `docs/architecture/FusionCore.md`

<br/>

## General configs

- logfile
    - default: "/var/log/discern.log"
    - A string path of the log file you'd like the server to store 
        any information about its operation

<br />

## gRPC Connection Options

- grpc_port
    - default: 50051
    - An integer value which will set the port the gRPC listeners will
        all listen on.

- listenip
    - default: localhost
    - A string value which is used to set the IP the gRPC server will
        listen on

- listenproto
    - default: tcp
    - A string value which is used to set the connection type gRPC 
        listen for. Technically an argument to net.Listen in golang

<br />

## inFlux Options

- influxport
    - default: 8086
    - An integer which sets the port number outside the docker container
        which the influx db port is forwarded to

- influxurl
    - default: 'http://localhost'
    - A string which contained to URL where the DB is hosted. This allows
        for the database to be hosted separately from the Core server, 
        without breaking anything

- influxtoken
    - default: 'BIGElHSa291FOkrliGaBVc7ksnGgQ4vALbkfJzRuH02T2XB8qouH0H3IkYTJACE-XZ-QYV664CH5655LkbQDIQ::'
    - A string which contains the database access token the Core sever
        needs to create a new client. If creating a new database is 
        specified, the token will be automatically added to the database

- influxdatamount:
    - default: discerninflux
    - A file path or volume where you'd like to store a persistent volume mount, so data can be stored
    through system reboots. You can also use this to mount old volumes into new containers.



- startnewinfluxinstance
    - default: true 
    - A boolean representing whether the previous docker instance should
        be killed and re-created on boot. Helpful for debugging

- bucket_name
    - default: DISCERN
    - A string used to set the bucket name in the influx instance

- org
    - default: ISI
    - A string used to set the org name in the influx instance

- influxuser 
    - default: default-user
    - A string used to set the default user name set up when creating
        the influx instance. This parameter has no effect if you do
        not used the built-in docker instance

- influxpassword
    - default: something
    - A string used to set the default password set for the default 
        user when creating the influx instance. This parameter has no 
        effect if you do not used the built-in docker instance

## Postgres Options

- postgresport
    - default: 5432
    - An integer which sets the port number outside the docker container
        which the postgres db port is forwarded to

- postgresip 
    - default: '127.0.0.1'
    - A string with ip where the DB is hosted. This allows
        for the database to be hosted separately from the Core server, 
        without breaking anything

- postgresdatamount
    - default: discernpsql
    - A file path or volume where you'd like to store a persistent volume mount, so data can be stored
    through system reboots. You can also use this to mount old volumes into new containers.

- startnewpostgresinstance
    - default: true 
    - A boolean representing whether the previous docker instance should
        be killed and re-created on boot. Helpful for debugging

- postgresuser
    - default: postgres
    - A string used to set the default user name set up when creating and logging into 
        the postgres db.

- postgrespassword
    - default: \*Something\*
    - A string used to set the default password set for the default 
        user when creating the postgres db.
