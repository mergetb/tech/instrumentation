package main;

import (
    "fmt";
    "time"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


func main() {
    config.LoadConfig()
    if !config.Settings.RunLog { return }

    err := log.InitializeLogOutput("Logs")
    if err != nil {
        fmt.Printf("error initializing logs logger: %v", err)
        return
    }

    err = ConnectToFusionCore()
    if err != nil {
        log.Fatal("failed to connect to fusion core: %v", err)
    }

    if config.Settings.LogToIntermediateFile {
        err = helpers.BuildDir(config.Settings.IntermediateDataFolder)
        if err != nil {
            log.Fatalf("error building intermediate data dir: %v", err)
        }
    }

    // Going to hold the most recent length measurement, so we don't repeat the data sent
    log_lengths := &map[string]int{};

    for {
        // Record dmesg stuff (HAS to be done separately)
        err = SaveDmesg(log_lengths)
        if err != nil {
            log.Errorf("error saving dmesg: %v", err)
        }

        // Record other log files
        for _, logFile := range config.Settings.LogFiles {
            err = SaveLog(logFile, log_lengths)
            if err != nil {
                log.Errorf("error saving %v: %v", logFile, err)
            }
        }

        log.Info("Sending has completed")
        
        time.Sleep(time.Duration(config.Settings.LogSweepInterval) * time.Second)
    }
}

