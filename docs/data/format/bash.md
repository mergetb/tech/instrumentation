# Saved Bash Data

Whenever a user `ssh`es into a machine and execute commands, the DataSorcerer will record the 
user's inputs and outputs, and sends them to the FusionCore.

The FusionCore stores all the following data into influx as a "bash" measurement:
    
- DevID
- User
- Host
- Count
- Cmds: User commands entered. The output is not saved.

`DevID` & `User` are tags

`Host`, `Count`, & `Cmds` are fields

Nothing is saved into the postgres database.

