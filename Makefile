.PHONY: all
all: FusionBridge FusionCore DataSorcerers

.PHONY: clean
clean:
	make -C FusionBridge clean
	make -C FusionCore clean
	make -C DataSorcerers clean


.PHONY: install
install: install/DataSorcerers install/FusionCore
	echo "Installing FusionCore and DataSorcerers"


.PHONY: stop-services
stop-services:
	make -C FusionCore stop-services
	make -C DataSorcerers stop-services


.PHONY: FusionBridge
FusionBridge:
	make -C FusionBridge all


.PHONY: FusionCore
FusionCore: FusionBridge
	make -C FusionCore build/FusionCore
	make -C FusionCore docker/influx
	make -C FusionCore build/influx
	make -C FusionCore docker/postgres
	make -C FusionCore build/postgres


.PHONY: install/FusionCore
install/FusionCore:
	make -C FusionCore install

.PHONY: DataSorcerers
DataSorcerers: FusionBridge
	make -C DataSorcerers all

.PHONY: enable
enable: install/DataSorcerers
	make -C DataSorcerers enable

.PHONY: install/DataSorcerers
install/DataSorcerers: DataSorcerers
	make -C DataSorcerers install


.PHONY: start/DataSorcerers
start/DataSorcerers: DataSorcerers enable
	echo "Starting Data Sorcerers"

.PHONY: testing
testing: FusionBridge
	make -C FusionCore    build/FusionCore
	make -C FusionCore    docker/postgres
	make -C FusionCore    docker/influx
	make -C FusionCore    docker/sandbox
	make -C DataSorcerers docker/discernpod

# To make life much easier
.PHONY: % fallback
%:
	(cd DataSorcerers && make $@) || (cd FusionCore && make $@) || (cd FusionBridge && make $@)

