package helpers;

import (
    "time"
    "google.golang.org/grpc" 
    "google.golang.org/grpc/credentials/insecure"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
)

func CreateConnection() *grpc.ClientConn {

    var opts []grpc.DialOption;
    opts = append(opts, 
        grpc.WithTimeout(20 * time.Second))
    opts = append(opts, 
        grpc.WithTransportCredentials(insecure.NewCredentials()));
   
    conn, err := grpc.Dial(config.Settings.ServerAddr(), opts...);
    if err != nil {
        log.Fatalf("failed to dial: %v", err)
    }

    return conn
}

