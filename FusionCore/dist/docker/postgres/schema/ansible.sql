CREATE TABLE IF NOT EXISTS AnsibleConfig (
    HASH TEXT UNIQUE,
    CONTENTS BYTEA
);

CREATE TABLE IF NOT EXISTS AnsiblePlaybook (
    HASH TEXT UNIQUE,
    CONTENTS BYTEA
);

