#!/bin/bash
/usr/local/bin/discern-ansible-sorcerer &
/usr/local/bin/discern-bash-sorcerer &
/usr/local/src/start_ttylog.sh &
/usr/local/bin/discern-file-sorcerer &
/usr/local/bin/discern-jupyter-sorcerer &
/usr/local/bin/discern-logs-sorcerer &
/usr/local/bin/discern-network-sorcerer &
/usr/local/bin/discern-os-sorcerer &
# /usr/local/bin/discern-bpftrace-sorcerer & # Doesn't REALLY make sense in docker container

