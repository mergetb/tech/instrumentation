# Save OS Metadata

There is a TON of OS metadata recorded. The Data Sorcerer utilizes bpftrace to record when 
systemcalls were called and what information was passed into them. This file is broken down by the 
different syscalls recorded and what information is kept.

Because these calls occur so quickly, they have a `Count` field, which tells you how many times a
syscall was used since the last time the data was recorded.

Some of these syscalls have Arg and ArgNum. This means these calls are passed a variable number of 
arguments, stored in ArgNum, and the actual arguments are stored in Arg, for you to parse out 
later.

Some of the following syscalls have the naming convention "Name(X)". Whenever you see a "(X)" 
after a syscall, that means this datatype is used to save multiple versions of the syscall. 
For example, we don't need different datatypes to save `RecvFrom` and `RecvMsg`.

All of this data is stored in influx in the measurement `os` with the `Call` tag identifying the 
particular systemcall.


## Close

Tags: 

- DevID
- Call

Fields:

- Pid (Process id)
- Uid (User id)
- Gid (Group id)
- Count
- Range (a bool to differentiate between Close and CloseRange syscalls)


## Execve(X)

Tags:

- DevID
- Arg
- ArgNum
- Call

Fields:

- Pid
- Uid
- Gid
- At: A bool to differentiate between Execvs vs ExecvsAt


## Fork

Tags:

- DevID
- Call

Fields:

- Pid
- Uid
- Gid
- Count


## Kill

Tags:

- DevID
- Sig: The kill signal passed to the process
- Call

Fields:

- Pid
- ArgPid
- Uid
- Gid


## Open(X)

Tags:

- DevID
- Filename
- Version: A string containing "", "At", "At2" to differentiate Open, OpenAt, OpenAt2
- Call

Fields:

- Pid
- Uid
- Gid
- Count

## Recv(X)

Tags:

- DevID
- Version: A string contianing "From", "MMsg", and "Msg" to differentiate RecvFrom, RecvMMsg, and RecvMsg
- Call

Fields:

- Pid
- Uid
- Gid
- FileDescriptor: Can be used to correlate communication, if you so desire
- Count


## Send(X)

Tags:

- DevID
- Version: A string containing "MMsg", "Msg", "To" to differential SendMMsg, SendMsg, and SendTo
- Call

Fields:

- Pid
- Uid
- Gid
- FileDescriptor: Can be used to correlate communication, if you so desire
- Len
- Count


## Socket(X)

Tags:

- DevID
- Family
- Version: A string containing "" or "Pair" to differential Socket vs SocketPair
- Call

Fields:

- Pid
- Uid
- Gid
- Count
- Type
- Protocol


## SysInfo

Tags:

- DevID
- Call

Fields:

- Pid
- Uid
- Gid


## TKill

Tags:

- DevID
- Sig: The kill signal passed
- Call

Fields:

- Pid
- Uid
- Gid
- ArgPid: The pid specified in the argument
- Count


## VFork

Tags:

- DevID
- Call

Fields:

- Pid
- Uid
- Gid
- Count

