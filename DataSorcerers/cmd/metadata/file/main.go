package main;

import (
    "fmt"
    "io/fs"
    "os"
    "os/user"
    "strings"
    "regexp"
    "path/filepath"
    "google.golang.org/grpc" 
    noti "github.com/fsnotify/fsnotify"


    bridge "FusionBridge/metadata/file"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


var opts []grpc.DialOption;
var conn *grpc.ClientConn;
var err error; 
var client bridge.FileClient;

var watcher *noti.Watcher;

func main() {
    config.LoadConfig()
    if !config.Settings.RunFile { return }

    err = log.InitializeLogOutput("File")
    if err != nil {
        fmt.Printf("error initializing file logger: %v", err)
        return
    }

    err = ConnectToFusionCore()
    if err != nil {
        log.Fatal("failed to connect to fusion core: %v", err)
    }

    if config.Settings.LogToIntermediateFile {
        err = helpers.BuildDir(config.Settings.IntermediateDataFolder)
        if err != nil {
            log.Fatalf("error building intermediate data dir: %v", err)
        }
    }

    foldersString := strings.Join(config.Settings.StartingDirs, ", ")

    log.Info("recursively adding listeners to folders: %v", foldersString)

    // Create fs watcher
    watcher, err = noti.NewWatcher()
    if err != nil {
        log.Fatalf("error adding watcher: %v", err)
    }
   
    for _, dir := range config.Settings.StartingDirs {
        // Add the watcher to the folder
        err = watcher.Add(dir)
        if err != nil {
            log.Errorf("error adding watcher: %v", err)
        }
    
        // Add watcher to all sub-folders
        err = filepath.Walk(dir, attachFolders); 
        if  err != nil {
            log.Errorf("error in attachFolders: %v", err)
        }
    }
   
    log.Info("listening for file updates")

    for {
        select {
            case ev := <-watcher.Events:
                err = SaveEvent(ev)
                if err != nil {
                    log.Errorf("error saving fs event: %v", err)
                } else {
                    log.Info("saved file data to core")
                }
            case err := <-watcher.Errors:
                log.Errorf("error while listening: %v", err)
        }
    }
}


func attachFolders(path string, fd os.FileInfo, err error) error {
     if err != nil {
        if pathErr, ok := err.(*fs.PathError); ok {
            if os.IsPermission(pathErr.Err) {
                log.Errorf("permission denied on: %v. passing", path)
                return nil
            }
        }

        return fmt.Errorf("crashed adding folder %v: %v", path, err)
    }
    
    // Only need to add the listeners to the directories
    if !fd.IsDir() { return nil }
    
    for _, regex := range config.Settings.BlackListDirs {
        if res, _ := regexp.MatchString(regex, path); res {
            // Prevent further descent if thats the case
            log.Infof("skipping blacklisted: %v", path)
            return filepath.SkipDir
        }
    }

    err = watcher.Add(path)
    if err != nil {
        return fmt.Errorf("error adding watcher to %v: %v", path, err)
    }
   
    return nil
}


func getUsernameFromUID(uid uint32) (string, error) {
    user, err := user.LookupId(fmt.Sprintf("%d", uid))
    if err != nil {
        return "", err
    }
    return user.Username, nil
}


func getGroupNameFromGID(gid uint32) (string, error) {
    group, err := user.LookupGroupId(fmt.Sprintf("%d", gid))
    if err != nil {
        return "", err
    }
    return group.Name, nil
}

