package influx;

import (

    "fmt"
    "time"
    "context"
    "os/exec"
    "strconv"
    "strings"
    log "github.com/sirupsen/logrus"

    "github.com/docker/docker/errdefs"
    "github.com/docker/docker/client"

    "github.com/influxdata/influxdb-client-go/v2"

    config "gitlab.com/instrumentation/FusionCore/pkg/config"

)

func StartDB() error {

    if !config.Settings.StartNewInfluxInstance { return nil; }

    // Don't catch errors cause it might not exist
    log.Info("stopping old influx container")
    _ = StopInfluxContainer()
    _ = RemoveInfluxContainer()

    err := CreatePersistentInfluxVolume()
    if err != nil {
        return fmt.Errorf("error creating persistent postgres volume: %v", err)
    }



    log.Info("starting up influx docker container")
    err = StartInfluxContainer()
    if err != nil {
        return fmt.Errorf("error starting influx docker container: %v", err)
    }

    err = WaitForInfluxContainerToStart()
    if err != nil {
        return fmt.Errorf("error waiting for influx container to start: %v", err)
    }

    return nil
}

func VerifyConnection(client influxdb2.Client) error {
    queryAPI := client.QueryAPI(config.Settings.ORG)

    _, err := queryAPI.Query(context.Background(), `from(bucket:"DISCERN")
        |> range(start: -1h) 
        |> filter(fn: (r) => r._measurement == "logs")`)

    if err != nil {
        return err
    }

    return nil
}

func GenerateClient() (influxdb2.Client, error) {
    addr := config.Settings.InfluxUrl + ":" + strconv.Itoa(int(config.Settings.InfluxPort))
    client := influxdb2.NewClient(addr, config.Settings.InfluxToken)

    _, err := client.Health(context.Background());
    for  err != nil {
        return nil, err
    }

    return client, nil
}

func StopInfluxContainer() error {
    args := strings.Fields("kill discerninflux")
    cmd := exec.Command("docker", args...)
    err := cmd.Run()
    return err
}

func RemoveInfluxContainer() error {
    args := strings.Fields("rm discerninflux")
    cmd := exec.Command("docker", args...)
    err := cmd.Run()
    return err
}

func CreatePersistentInfluxVolume() error {
    // Check if container exists
    args := strings.Fields("volume inspect discerninflux")
    cmd := exec.Command("docker", args...)
    err := cmd.Run()
    // Check if volume exists
    if err == nil { 
        return nil 
    }

    args = strings.Fields("volume create discerninflux")
    cmd = exec.Command("docker", args...)
    return cmd.Run()
}

func StartInfluxContainer() error {
    port := strconv.Itoa(int(config.Settings.InfluxPort))
    mount := config.Settings.InfluxDataMount
    args := strings.Fields("run --name discerninflux -p "+ "8086:" + port + 
                            " --restart unless-stopped" +
                            " -v " + mount + ":/var/lib/influxdb2 discerninflux")
    cmd := exec.Command("docker", args...)
    err := cmd.Start()
    return err
}

func WaitForInfluxContainerToStart() error {
    // Create a new client to figure out when the container starts
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        return fmt.Errorf("error creating NewClient for influx container: %v", err)
    }

    containerRunning := false;

    for !containerRunning {
        log.Info("waiting on influxDB container")
        containerInfo, err := cli.ContainerInspect(context.Background(), "discerninflux")

        if err != nil {
            if _, ok := err.(errdefs.ErrNotFound); !ok { // If the container is not found
                return fmt.Errorf("error inspecting influxDB container %v", err)
            }

        } else { // Only check if theres not an error otherwise nil pointer
            if containerInfo.State.Running {
                containerRunning = true;
            }
        }

        time.Sleep(1 * time.Second)
    }

    return nil
}
