package main;

import (
    "os"
    "fmt"
    "regexp"
    "path/filepath"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
)

func ScrapePlaybooks() {
    log.Info("Saving playbooks from /etc/ansible")

    if _, err := os.Stat("/etc/ansible"); os.IsNotExist(err) || err != nil { 
        if err != nil {
            log.Errorf("error reading /etc/ansible: %v", err)
        }
        return
    }

    if err := filepath.Walk("/etc/ansible", WalkForPlaybooks); err != nil {
        log.Errorf("error scraping playbooks: %v", err)
    }

    return
}

func WalkForPlaybooks(path string, fd os.FileInfo, err error) error {
    if err != nil {
        return err
    }

    if fd.IsDir() { return nil }

    _, err = regexp.MatchString(`\.ya?ml$`, fd.Name())

    if err != nil {
        return fmt.Errorf("regex failure finding playbooks: %v", err)
    }
    
    err = SaveYaml(path);
    if err != nil {
        return fmt.Errorf("error saving playbook %v: %v", path, err)
    }

    return nil
}

