package main;

import (
    "context"
    "time"

    bridge "FusionBridge/validation"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


func main() {

    config.LoadConfig()


    conn := helpers.CreateConnection()
    client := bridge.NewSendAndRecvClient(conn); 

    err := log.InitializeLogOutput("Validation")
    if err != nil {
        log.Fatalf("error ", err)
    }

    ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
    defer cancel()

    to_send := bridge.HelloWorld{ Tmp : 12345 }

    msg, err := client.HelloWorldBasic(ctx, &to_send)
    if err != nil {
        log.Fatalf("Error running validation: %v", err)
    }

    log.Infof("Client got response: %v", msg.Tmp)
}

