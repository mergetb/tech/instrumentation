# Saved Log Data

The FusionCore saves the raw log data into the postgres database with a hash as a foreign key with 
the influx database. 

You can find the postgres schema at `FusionCore/dist/docker/postgres/schema/log.sql`

`Location`, `DevID`, `ContentHash` are saved to the influx database as "log" measurements and 
`Location` is where the log was read from.

`Location` & `DevID` are tags

`ContentHash` is a field

