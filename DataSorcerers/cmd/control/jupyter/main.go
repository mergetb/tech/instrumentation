package main;

import (
    "os"
    "fmt"
    "time"
    "regexp"
    "path/filepath"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)

func main() {
    config.LoadConfig()
    if !config.Settings.RunJupyter { return }

    err := log.InitializeLogOutput("Jupyter")
    if err != nil {
        fmt.Errorf("error ", err)
        return
    }

    err = ConnectToFusionCore()
    if err != nil {
        log.Fatal("failed to connect to fusion core: %v", err)
    }

    if config.Settings.LogToIntermediateFile {
        err = helpers.BuildDir(config.Settings.IntermediateDataFolder)
        if err != nil {
            log.Fatalf("error building intermediate data dir: %v", err)
        }
    }

    for {
        for _, path := range config.Settings.JupyterPaths {
            if err := filepath.Walk(path, walkFn); err != nil {
                log.Infof("error walking for playbooks: %v", err)
            }
        }

        log.Info("File search & saving stopped")

        time.Sleep(time.Duration(config.Settings.JupyterSweepInterval) * time.Second)
    }
}

func walkFn(path string, fd os.FileInfo, err error) error {
    if err != nil {
        return fmt.Errorf("error walking: %v", err)
    }

    if fd.IsDir() { return nil }

    res, err := regexp.MatchString(`.*\.ipynb$`, fd.Name())
    if err != nil {
        return fmt.Errorf("error regex-ing for playbook: %v", err)
    }

    if !res { return nil }

    err = SaveData(path);
    if err != nil {
        return fmt.Errorf("error saving playbook: %v", err)
    }

    return nil
}

