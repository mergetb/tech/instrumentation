package os;

import (
    "sync"
    "context"
    "strconv"
    log "github.com/sirupsen/logrus"
    "github.com/influxdata/influxdb-client-go/v2"
    "github.com/influxdata/influxdb-client-go/v2/api"
   
    bridge "FusionBridge/metadata/os"

    "gitlab.com/instrumentation/FusionCore/pkg/config"
)

type OSServer struct {
    bridge.UnimplementedOSServer
    mu sync.Mutex
    writeAPI api.WriteAPIBlocking
    queryAPI api.QueryAPI
}

func NewServer(client influxdb2.Client) *OSServer {
    s := &OSServer{
        writeAPI: client.WriteAPIBlocking(config.Settings.ORG, config.Settings.BUCKET_NAME),
        queryAPI: client.QueryAPI(config.Settings.ORG),
    }
    return s
}


func (s *OSServer) MarkCloseSysCall(ctx context.Context, 
    MSG *bridge.CloseCall) (*bridge.OS_ACK, error) {

    call := "SysClose"
    if MSG.Range == true { 
        call += "Range" 
    }

    tags := map[string]string{
           "DevID":MSG.DevID,
           "Call": "SysClose",
    }
    fields := map[string]interface{}{
           "Pid":MSG.Pid,           
           "Uid":MSG.Uid,
           "Gid":MSG.Gid,
           "Count":MSG.Count,
           "Range":false, // Place Close and Close range into same index
    }


    point := influxdb2.NewPoint("os", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    
    if err != nil {
        log.Infof("error in network.LogNetworkActivity: %v", err)
        return &bridge.OS_ACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }
 
    return &bridge.OS_ACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}


func (s *OSServer) MarkExecveSysCall(ctx context.Context, 
    MSG *bridge.ExecveCall) (*bridge.OS_ACK, error) {

    Call := "Execve"
    if MSG.At { Call += "At" }

    tags := map[string]string{
           "DevID":MSG.DevID,
           "Arg":MSG.Arg,
           "ArgNum":strconv.Itoa(int(MSG.ArgNum)),
           "Call": Call,
    }

    fields := map[string]interface{}{
           "Pid":MSG.Pid,           
           "Uid":MSG.Uid,
           "Gid":MSG.Gid,
           "At":false, // Place Close and Close range into same index
    }

    point := influxdb2.NewPoint("os", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    
    if err != nil {
        log.Infof("error in os.MarkExecveSysCall: %v", err)
        return &bridge.OS_ACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }
 
    return &bridge.OS_ACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}


func (s *OSServer) MarkForkSysCall(ctx context.Context, 
    MSG *bridge.ForkCall) (*bridge.OS_ACK, error) {

    tags := map[string]string{
           "DevID":MSG.DevID,
           "Call": "Fork",
    }
    fields := map[string]interface{}{
           "Pid":MSG.Pid,           
           "Uid":MSG.Uid,
           "Gid":MSG.Gid,
           "Count":MSG.Count,
    }


    point := influxdb2.NewPoint("os", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    
    if err != nil {
        log.Infof("error in os.Fork: %v", err)
        return &bridge.OS_ACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }
 
    return &bridge.OS_ACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}


func (s *OSServer) MarkKillSysCall(ctx context.Context, 
    MSG *bridge.KillCall) (*bridge.OS_ACK, error) {

    Call := "Kill"
    if MSG.T { Call = "T" + Call; }

    tags := map[string]string{
           "DevID":MSG.DevID,
           "Sig":  strconv.Itoa(int(MSG.Sig)),
           "Call": Call,
    }
    fields := map[string]interface{}{
           "Pid":MSG.Pid,           
           "ArgPid":MSG.ArgPid,           
           "Uid":MSG.Uid,
           "Gid":MSG.Gid,
    }


    point := influxdb2.NewPoint("os", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    
    if err != nil {
        log.Infof("error in os.Kill: %v", err)
        return &bridge.OS_ACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }
 
    return &bridge.OS_ACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}


func (s *OSServer) MarkOpenSysCall(ctx context.Context, 
    MSG *bridge.OpenCall) (*bridge.OS_ACK, error) {

    Call := "Open"
    if MSG.Variation == "at" {
        Call += "At"
    } else if MSG.Variation == "at2" {
        Call += "At2"
    }


    tags := map[string]string{
           "DevID":MSG.DevID,
           "Filename":MSG.Filename,
           "Version":"",
           "Call": Call,
    }
    fields := map[string]interface{}{
           "Pid":MSG.Pid,
           "Uid":MSG.Uid,
           "Gid":MSG.Gid,
           "Count":MSG.Count,
    }


    point := influxdb2.NewPoint("os", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    
    if err != nil {
        log.Infof("error in os.Open: %v", err)
        return &bridge.OS_ACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }
 
    return &bridge.OS_ACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}


func (s *OSServer) MarkRecvSysCall(ctx context.Context, 
    MSG *bridge.RecvCall) (*bridge.OS_ACK, error) {

    Call := "From" + MSG.Variation

    tags := map[string]string{
           "DevID":MSG.DevID,
           "Version":"From",
           "Call": Call,
    }

    fields := map[string]interface{}{
           "Pid":MSG.Pid,
           "Uid":MSG.Uid,
           "Gid":MSG.Gid,
           "FileDescriptor":MSG.FileDescriptor,
           "Count":MSG.Count,
    }


    point := influxdb2.NewPoint("os", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    
    if err != nil {
        log.Infof("error in os.Recv: %v", err)
        return &bridge.OS_ACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }
 
    return &bridge.OS_ACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}


func (s *OSServer) MarkSendSysCall(ctx context.Context, 
    MSG *bridge.SendCall) (*bridge.OS_ACK, error) {

    Call := "Send" + MSG.Variation

    tags := map[string]string{
           "DevID":MSG.DevID,
           "Version":"MMsg",
           "Call": Call,
    }
    fields := map[string]interface{}{
           "Pid":MSG.Pid,
           "Uid":MSG.Uid,
           "Gid":MSG.Gid,
           "FileDescriptor":MSG.FileDescriptor,
           "Len":MSG.Len,
           "Count":MSG.Count,
    }


    point := influxdb2.NewPoint("os", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    
    if err != nil {
        log.Infof("error in os.Send: %v", err)
        return &bridge.OS_ACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }
 
    return &bridge.OS_ACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}

func (s *OSServer) MarkSocketSysCall(ctx context.Context, 
    MSG *bridge.SocketCall) (*bridge.OS_ACK, error) {

    Call := "Socket"
    if MSG.Pair { Call += "Pair" }

    tags := map[string]string{
           "DevID":MSG.DevID,
           "Version":"",
           "Family":strconv.Itoa(int(MSG.Family)),
           "Call": "Socket",
    }
    fields := map[string]interface{}{
           "Pid":MSG.Pid,
           "Uid":MSG.Uid,
           "Gid":MSG.Gid,
           "Count":MSG.Count,
           "Protocol":MSG.Protocol,
           "Type":MSG.Type,
    }


    point := influxdb2.NewPoint("os", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    
    if err != nil {
        log.Infof("error in os.Socket: %v", err)
        return &bridge.OS_ACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }
 
    return &bridge.OS_ACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}

func (s *OSServer) MarkSysInfoSysCall(ctx context.Context, 
    MSG *bridge.SysInfoCall) (*bridge.OS_ACK, error) {

    tags := map[string]string{
           "DevID":MSG.DevID,
           "Call": "SysInfo",
    }

    fields := map[string]interface{}{
           "Pid":MSG.Pid,
           "Uid":MSG.Uid,
           "Gid":MSG.Gid,
    }

    point := influxdb2.NewPoint("os", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    
    if err != nil {
        log.Infof("error in os.SysInfo: %v", err)
        return &bridge.OS_ACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }
 
    return &bridge.OS_ACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}

