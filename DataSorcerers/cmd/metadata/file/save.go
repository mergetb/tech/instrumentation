package main;

import (
    "os"
    "fmt"
    "time"
    "context"
    "syscall"
    "github.com/golang/protobuf/ptypes"
    noti "github.com/fsnotify/fsnotify"


    bridge "FusionBridge/metadata/file"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


func ConnectToFusionCore() error {
    conn = helpers.CreateConnection()
    client = bridge.NewFileClient(conn); 

    return nil
}


func SaveEvent(ev noti.Event) error {
    ctx, cancel := context.WithTimeout(context.Background(), 
        10*time.Second)
    defer cancel()

    timestamp, _ := ptypes.TimestampProto(time.Now())

    MSG := bridge.FsEvent{
        SubmissionNumber:0,
        TimeStamp:timestamp,
        Op: int32(ev.Op),
        Location: string(ev.Name),
        DevID: config.GetID(),
    }

    // Decided against saving file data, but left here for support
    /*
    // Save file content updates
    if (ev.Op == noti.Write) { 
        data, err := ioutil.ReadFile(string(ev.Name)) 
        if err == nil { 
            MSG.Content = data
        } else {
            log.Errorf("error reading file while saving event: %v", err)
        }
    }
    */

    // Save 
    if ev.Op == noti.Create || ev.Op == noti.Chmod {
        // Strip out permissions information
        fileInfo, err := os.Stat(string(ev.Name))
        if err != nil {
            log.Errorf("error getting file info for: %v; %v", string(ev.Name), err)
        } else {
            // Make sure we can get the fileInfo. This su
            perms := fileInfo.Mode().Perm()
            MSG.Permissions = uint32(perms)
       
            // Get the user owners
            owner := fileInfo.Sys().(*syscall.Stat_t).Uid // Owner UID
            ownerName, err := getUsernameFromUID(owner)
            if err != nil {
                log.Errorf("error getting file owner name: %v", err)
            } else {
                MSG.Owner = ownerName
            }

            // Get the group owner
            group := fileInfo.Sys().(*syscall.Stat_t).Gid // Group GID
            groupName, err := getGroupNameFromGID(group)
            if err != nil {
                log.Errorf("error getting group name: %v", err)
            } else {
                MSG.Group = groupName
            }
        }
    }

    e := helpers.SaveToIntermediate(&MSG, "file")
    if e != nil {
        log.Errorf("error logging intermediate data to file: %v", e)
    }

    _, e = client.SaveFsEvent(ctx, &MSG)
    if e != nil {
        return fmt.Errorf("error saving fs event: %v", e)
    }

    return nil
}

