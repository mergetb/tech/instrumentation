package postgres;

import (

    "fmt"
    "context"
    "os"
    "os/exec"
    "strings"
    "time"
    
    "github.com/jackc/pgx/v5/pgxpool"

    "strconv"

    "github.com/docker/docker/errdefs"
    "github.com/docker/docker/client"
    log "github.com/sirupsen/logrus"

    config "gitlab.com/instrumentation/FusionCore/pkg/config"

)

var Connection *pgxpool.Pool;
var err error;

func StartDB() error {

    if !config.Settings.StartNewPostgresInstance { return nil; }

    old_dir, err := os.Getwd()
    if err != nil {
        return fmt.Errorf("Error getting current working directory: %v", err)
    }

    err = os.Chdir("/etc/discern/postgres")
    if err != nil {
        return fmt.Errorf("Error changing working directory: %v", err)
    }
    defer os.Chdir(old_dir) // Is this technically bad?

    log.Info("stopping old postgres container")
    _ = StopPostgresDatabase()
    _ = RemovePostgresDatabase()

    // Allow for persistent storage throughout reboots
    // Users are expected to manage this themselves now
    // _ = RemovePersistentPostgresVolume()

    err = CreatePersistentPostgresVolume()
    if err != nil {
        return fmt.Errorf("error creating persistent postgres volume: %v", err)
    }

    log.Info("starting up postgres docker container")
    err = StartPostgresDockerContainer()
    if err != nil {
        return err // We use output from cmd in this one so we craft it before the return
    }

    err = WaitForPostgresContainerToStart()
    if err != nil {
        return fmt.Errorf("error waiting for postgres to start: %v", err)
    }

    return nil
}

func Connect() error {
    // Kept as default
    connStr := fmt.Sprintf("postgresql://%v:%v@%v/Discern?sslmode=disable", 
        config.Settings.PostgresUser, config.Settings.PostgresPassword, 
        config.Settings.PostgresIP)

    Connection, err = pgxpool.New(context.Background(), connStr)

    return err
}

func VerifyConnection() error {
    rows, err := Connection.Query(context.Background(), `SELECT * FROM Logs;`)
    if err != nil {
        return fmt.Errorf("error creating query: %v", err)
    }
    defer rows.Close()

    for rows.Next() {
        // Get field descriptions and column values dynamically
        values, err := rows.Values()
        if err != nil {
            return fmt.Errorf("error reading values of query: %v", err)
        }

        // Print each value
        for i, v := range values {
                fmt.Printf("Column %d: %v\n", i+1, v)
        }
        fmt.Println("-------------------")
    }

    if err := rows.Err(); err != nil {
        return fmt.Errorf("query reading ended in error: %v", err)
    }

    return nil
}

func StopPostgresDatabase() error {
    args := strings.Fields("stop discernpsql")
    cmd := exec.Command("docker", args...)
    return cmd.Run()
}

func RemovePostgresDatabase() error {
    args := strings.Fields("rm discernpsql")
    cmd := exec.Command("docker", args...)
    return cmd.Run()
}

func RemovePersistentPostgresVolume() error {
    args := strings.Fields("volume rm discernpsql")
    cmd := exec.Command("docker", args...)
    return cmd.Run()
}

func CreatePersistentPostgresVolume() error {
    // Check if container exists
    args := strings.Fields("volume inspect discernpsql")
    cmd := exec.Command("docker", args...)
    err := cmd.Run()
    // Check if volume exists
    if err == nil { 
        return nil 
    }

    args = strings.Fields("volume create discernpsql")
    cmd = exec.Command("docker", args...)
    return cmd.Run()
}

func StartPostgresDockerContainer() error {
    // Start up the image
    port := strconv.FormatInt(config.Settings.PostgresPort, 10)
    mount := config.Settings.PostgresDataMount
    args := strings.Fields("run -dit -p 5432:" + port + 
                            " --restart unless-stopped" +
                            " -v " + mount + ":/var/lib/postgresql/data" + // config.Settings.PostgresDataMount + 
                            " --name discernpsql discernpsql")

    cmd := exec.Command("docker", args...)

    // Capture both stdout and stderr
    output, err := cmd.CombinedOutput()
    if err != nil {
        return fmt.Errorf("error starting postgres container: %v\nOutput:\n%s", err, string(output))
    }

    return nil
}

func WaitForPostgresContainerToStart() error {
    // Create a new client to figure out when the container starts
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        return fmt.Errorf("error creating env client for postgres: %v", err)
    }

    containerRunning := false;

    for !containerRunning {
        log.Info("waiting on postgres container")
        // Use the Docker client to inspect the container
        containerInfo, err := cli.ContainerInspect(context.Background(), "discernpsql")

        if err != nil {
            // If the container is not found, an error is returned
            if _, ok := err.(errdefs.ErrNotFound); !ok {
                return fmt.Errorf("error inspecting postgres container %v", err)
            }
        } else { // Only check if theres not an error otherwise nil pointer
            if containerInfo.State.Running {
                containerRunning = true;
            }
        }

        time.Sleep(1 * time.Second)
    } 

    return nil
}
