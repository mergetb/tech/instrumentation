package main;

import (
    "os"
    "fmt"
    "regexp"
    "strings"
    "path/filepath"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
)

func ScrapeConfigs() {
    // Save #1
    log.Info("saving config specified in env")
    configLoc := strings.TrimSpace(os.Getenv("ANSIBLE_CONFIG"))

    if configLoc != "" {  // If we actually got a config from the env
        err := SaveConfig(configLoc)
        if err != nil {
            log.Errorf("error saving config specified in env: %v", err)
        }
    }
    

    // Save #2
    log.Info("saving config and playbooks from home")
    // Actually iterate over every file in the directory and save it if its a config or a playbook
    err := filepath.Walk("/home", WalkForConfigs);
    if err != nil {
        log.Errorf("error while scraping home: %v", err)
    }


    // Save #3 & #4
    log.Info("saving config from ~ and /etc")    

    err = SaveConfig("~/ansible.cfg")
    if err != nil {
        log.Errorf("error saving config from ~: %v", err)
    }

    err = SaveConfig("/etc/ansible/ansible.cfg")
    if err != nil {
        log.Errorf("error saving config from /etc: %v", err)
    }
}

func WalkForConfigs(path string, fd os.FileInfo, err error) error {
    if err != nil {
        return err
    }

    if fd.IsDir() { return nil }

    // Find ansible configs
    res, err := regexp.MatchString(`ansible.cfg$`, fd.Name())
    if err != nil {
        return fmt.Errorf("error regexing for ansible configs: %v", err)
    }

    // If theres anything in it, save it
    if res { 
        err = SaveConfig(path);
        if err != nil {
            return fmt.Errorf("error saving config %v in home: %v", path, err)
        }
    }

    // Find the playbooks
    res, err = regexp.MatchString(`\.ya?ml$`, fd.Name())
    if err != nil {
        return fmt.Errorf("error regexing for ansible configs: %v", err)
    }

    // If its empty, leave without issue
    if !res { 
        return nil
    }

    // Save the playbooks
    err = SaveYaml(path);
    if err != nil {
        return fmt.Errorf("error saving yaml %v in home: %v", path, err)
    }

    return nil
}

