package save;

import (
    "fmt"
    "time"
    "strings"
    "strconv"
    "encoding/json"
    "github.com/golang/protobuf/ptypes"
    "github.com/golang/protobuf/ptypes/timestamp"
)


// Structure to hold timestamps generated in our bpftrace data
type JsonTimeStamp struct {
    Time *timestamp.Timestamp
}


// Generic type to reduce re-writing
type Data struct {
    At map[string]interface{} `json:"@"`
}


// Type alias to make conversion from int64 to grpc time stamp cleaner
type  Nsec int64

func (j Nsec) ToGRPC() (*timestamp.Timestamp, error) {
    // Define the Unix epoch as a reference time
    epoch := time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)

    // Calculate the timestamp by adding nanoseconds to the Unix epoch
    timestamp := epoch.Add(time.Duration(j))

    gRPC_timestamp, err := ptypes.TimestampProto(timestamp)
    if err != nil {
        return nil, fmt.Errorf("could not create timestamp: %v", err);
    }

    return gRPC_timestamp, nil
}


// Data structures needed to parse Close syscalls output json
type CloseRootJson struct {
    Type string    `json:"type"`
    Data JsonCloseData `json:"data"`
    Range bool
    TimeStamps []*JsonTimeStamp
}

type JsonCloseData []Close;

type Close struct {
    Pid int32
    Uid int32
    Gid int32
    Count int32
}


// Data structures needed to parse SysInfo syscalls output json
type SysInfoRootJson struct {
    Type string    `json:"type"`
    Data JsonSysInfoData `json:"data"`
}

type JsonSysInfoData []SysInfo;

type SysInfo struct {
    Pid  int32
    Uid  int32
    Gid  int32
    Nsec Nsec
}


// Data structures needed to parse Execve syscalls output json
type ExecveRootJson struct {
    Type string `json:"type"`
    Data ExecveData `json:"data"`
    At     bool
}

type ExecveData []Execve;

type Execve struct {
    Pid    int32
    Uid    int32
    Gid    int32
    ArgNum int32
    Arg    string
    Nsec   Nsec
}


// Data structures needed to parse Execve syscalls output json
type ForkRootJson struct {
    Type string `json:"type"`
    Data ForkData `json:"data"`
    V      bool
    TimeStamps []*JsonTimeStamp
}

type ForkData []Fork;

type Fork struct {
    Pid    int32
    Uid    int32
    Gid    int32
    Tid    int32
    Count  int32
}


// Data structures needed to parse Kill syscalls output json
type KillRootJson struct {
    Type string   `json:"type"`
    Data KillData `json:"data"`
    T    bool
    TimeStamps []*JsonTimeStamp
}

type KillData []Kill;

type Kill struct {
    Pid    int32
    Uid    int32
    Gid    int32
    ArgPid int32
    Sig    int32
    Count  int32
}


// Data structures needed to parse Open syscalls output json
type OpenRootJson struct {
    Type string   `json:"type"`
    Data OpenData `json:"data"`
    Variation string
    TimeStamps []*JsonTimeStamp
}

type OpenData []Open;

type Open struct {
    Pid       int32
    Uid       int32
    Gid       int32
    Filename  string
    Count     int32
}


// Data structures needed to parse Open syscalls output json
type RecvRootJson struct {
    Type string   `json:"type"`
    Data RecvData `json:"data"`
    Variation string
    TimeStamps []*JsonTimeStamp
}

type RecvData []Recv;

type Recv struct {
    Pid       int32
    Uid       int32
    Gid       int32
    Fd        int32
    Count     int32
}


// Data structures needed to parse Open syscalls output json
type SendRootJson struct {
    Type string   `json:"type"`
    Data SendData `json:"data"`
    Variation string
    TimeStamps []*JsonTimeStamp
}

type SendData []Send;

type Send struct {
    Pid       int32
    Uid       int32
    Gid       int32
    Fd        int32
    Len       int32
    Count     int32
}


// Data structures needed to parse Open syscalls output json
type SocketRootJson struct {
    Type string   `json:"type"`
    Data SocketData `json:"data"`
    Pair bool
    TimeStamps []*JsonTimeStamp
}

type SocketData []Socket;

type Socket struct {
    Pid       int32
    Uid       int32
    Gid       int32
    Family    int32
    Type      int32
    Protocol  int32
    Count     int32
}



// *** How to unmarshal the json files into these types. Tricky parses only. Rest is auto gen ***

func (d *JsonTimeStamp) UnmarshalJSON(b []byte) error {
    var tmp struct {
        Type      string `json:"type"`
        TimeStamp string `json:"data"`
    }

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &tmp); err != nil {
        return err
    }

    if  tmp.Type != "time" {
        return fmt.Errorf("unmarshaled object is not a time stamp")
    }

    layout := "15:04:05"
    input := strings.TrimSpace(tmp.TimeStamp)

    // Parse the time using the layout
    parsedTime, err := time.Parse(layout, input)
    if err != nil {
        return fmt.Errorf("error parsing timestamp: %v", err)
    }

    // Set the date to today, preserving the parsed time
    now := time.Now()
    t := time.Date(now.Year(), now.Month(), now.Day(), parsedTime.Hour(), parsedTime.Minute(), parsedTime.Second(), 0, time.Local)

    // Convert to a protobuf timestamp
    timestamp, err := ptypes.TimestampProto(t)
    if err != nil {
        return fmt.Errorf("error creating timestamp: %v", err)
    }

    d.Time = timestamp

    return nil
}



func (d *JsonCloseData) UnmarshalJSON(b []byte) error {
    ret := []Close{};

    // Create a temporary structure to match the JSON structure
    var temp Data;

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &temp); err != nil {
        return fmt.Errorf("error unmarshalling '%v': %v", string(b), err)
    }

    for key, interf := range temp.At {

        value, ok := interf.(float64)
        if !ok {
            return fmt.Errorf("error paring count. not an float64")
        }

        els := strings.Split(key, ",")

        if len(els) != 3 {
            return fmt.Errorf("error parsing close data. key doesn't have 4 elements")
        }

        pid, err := strconv.Atoi(els[0])
        if err != nil {
            return fmt.Errorf("error parsing pid to int: %v", err)
        }

        uid, err := strconv.Atoi(els[1])
        if err != nil {
            return fmt.Errorf("error parsing uid to int: %v", err)
        }

        gid, err := strconv.Atoi(els[2])
        if err != nil {
            return fmt.Errorf("error parsing gid to int: %v", err)
        }

        ret = append(ret, Close{
            Pid:   int32(pid),
            Uid:   int32(uid),
            Gid:   int32(gid),
            Count: int32(value),
        })
    }

    *d = ret

    // Assign the parsed data to the actual struct
    return nil
}


func (d *JsonSysInfoData) UnmarshalJSON(b []byte) error {
    ret := []SysInfo{};

    // Create a temporary structure to match the JSON structure
    var temp Data

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &temp); err != nil {
        return fmt.Errorf("error unmarshalling '%v': %v", string(b), err)
    }

    for key, interf := range temp.At {

        value, ok := interf.(float64)
        if !ok {
            return fmt.Errorf("nsec cannot be used as an float64")
        }

        els := strings.Split(key, ",")

        if len(els) != 3 {
            return fmt.Errorf("error parsing SysInfo data. key doesn't have 4 elements")
        }

        pid, err := strconv.Atoi(els[0])
        if err != nil {
            return fmt.Errorf("error parsing pid to int: %v", err)
        }

        uid, err := strconv.Atoi(els[1])
        if err != nil {
            return fmt.Errorf("error parsing uid to int: %v", err)
        }

        gid, err := strconv.Atoi(els[2])
        if err != nil {
            return fmt.Errorf("error parsing gid to int: %v", err)
        }

        ret = append(ret, SysInfo{
            Pid:   int32(pid),
            Uid:   int32(uid),
            Gid:   int32(gid),
            Nsec:  Nsec(value),
        })
    }

    *d = ret

    // Assign the parsed data to the actual struct
    return nil
}


func (d *ExecveData) UnmarshalJSON(b []byte) error {
    ret := []Execve{};

    // Create a temporary structure to match the JSON structure
    var temp Data

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &temp); err != nil {
        return fmt.Errorf("error unmarshalling '%v': %v", string(b), err)
    }

    for key, interf := range temp.At {

        value, ok := interf.(string)
        if !ok {
            return fmt.Errorf("arg section of json was not string")
        }

        els := strings.Split(key, ",")

        if len(els) != 4 {
            return fmt.Errorf("error parsing execve data. key doesn't have 4 elements")
        }

       
        pid, err := (strconv.Atoi(els[0]))
        if err != nil {
            return fmt.Errorf("ExecveData Failed to convert pid %v to int", els[0])
        }

        nsec, err := (strconv.Atoi(els[1]))
        if err != nil {
            return fmt.Errorf("ExecveData Failed to convert nsecs %v to int", els[1])
        }

        uid, err := (strconv.Atoi(els[2]))
        if err != nil {
            return fmt.Errorf("ExecveData Failed to convert uid %v to int", els[2])
        }

        gid, err := (strconv.Atoi(els[3]))
        if err != nil {
            return fmt.Errorf("ExecveData Failed to convert gid %v to int", els[3])
        }

        argNum, err := (strconv.Atoi(strings.TrimSpace(els[4])))
        if err != nil {
            return fmt.Errorf("ExecveData Failed to convert argNum %v to int", els[4])
        }

        ret = append(ret, Execve{
            Pid:    int32(pid),
            Nsec:   Nsec(nsec),
            Uid:    int32(uid),
            Gid:    int32(gid),
            ArgNum: int32(argNum),
            Arg:    value,
        })
    }

    *d = ret

    return nil
}


func (d *ForkData) UnmarshalJSON(b []byte) error {
    ret := []Fork{};

    // Create a temporary structure to match the JSON structure
    var temp Data;

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &temp); err != nil {
        return fmt.Errorf("error unmarshalling '%v': %v", string(b), err)
    }

    for key, interf := range temp.At {

        value, ok := interf.(float64)
        if !ok {
            return fmt.Errorf("error paring count. not an float64")
        }

        els := strings.Split(key, ",")

        if len(els) != 4 {
            return fmt.Errorf("error parsing fork data. key doesn't have 4 elements")
        }

        pid, err := strconv.Atoi(els[0])
        if err != nil {
            return fmt.Errorf("error parsing pid to int: %v", err)
        }

        uid, err := strconv.Atoi(els[1])
        if err != nil {
            return fmt.Errorf("error parsing uid to int: %v", err)
        }

        gid, err := strconv.Atoi(els[2])
        if err != nil {
            return fmt.Errorf("error parsing gid to int: %v", err)
        }

        tid, err := strconv.Atoi(els[3])
        if err != nil {
            return fmt.Errorf("error parsing tid to int: %v", err)
        }

        ret = append(ret, Fork{
            Pid:   int32(pid),
            Uid:   int32(uid),
            Gid:   int32(gid),
            Tid:   int32(tid),
            Count: int32(value),
        })
    }

    *d = ret

    // Assign the parsed data to the actual struct
    return nil
}


func (d *KillData) UnmarshalJSON(b []byte) error {
    ret := KillData{};

    // Create a temporary structure to match the JSON structure
    var temp Data;

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &temp); err != nil {
        return fmt.Errorf("error unmarshalling '%v': %v", string(b), err)
    }

    for key, interf := range temp.At {

        value, ok := interf.(float64)
        if !ok {
            return fmt.Errorf("error paring count. not an float64")
        }

        els := strings.Split(key, ",")

        if len(els) != 5 {
            return fmt.Errorf("error parsing kill data. key doesn't have 5 elements")
        }

        pid, err := strconv.Atoi(els[0])
        if err != nil {
            return fmt.Errorf("error parsing pid to int: %v", err)
        }

        uid, err := strconv.Atoi(els[1])
        if err != nil {
            return fmt.Errorf("error parsing uid to int: %v", err)
        }

        gid, err := strconv.Atoi(els[2])
        if err != nil {
            return fmt.Errorf("error parsing gid to int: %v", err)
        }

        tpid, err := strconv.Atoi(els[3])
        if err != nil {
            return fmt.Errorf("error parsing tpid to int: %v", err)
        }

        sig, err := strconv.Atoi(els[4])
        if err != nil {
            return fmt.Errorf("error parsing sig to int: %v", err)
        }

        ret = append(ret, Kill{
            Pid:   int32(pid),
            Uid:   int32(uid),
            Gid:   int32(gid),
            ArgPid:  int32(tpid),
            Sig:   int32(sig),
            Count: int32(value),
        })
    }

    *d = ret

    // Assign the parsed data to the actual struct
    return nil
}

func (d *OpenData) UnmarshalJSON(b []byte) error {
    ret := OpenData{};

    // Create a temporary structure to match the JSON structure
    var temp Data;

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &temp); err != nil {
        return fmt.Errorf("error unmarshalling '%v': %v", string(b), err)
    }

    for key, interf := range temp.At {

        value, ok := interf.(float64)
        if !ok {
            return fmt.Errorf("error paring count. not an float64")
        }

        els := strings.Split(key, ",")

        if len(els) != 4 {
            return fmt.Errorf("error parsing kill data. key doesn't have 4 elements")
        }

        pid, err := strconv.Atoi(els[0])
        if err != nil {
            return fmt.Errorf("error parsing pid to int: %v", err)
        }

        uid, err := strconv.Atoi(els[1])
        if err != nil {
            return fmt.Errorf("error parsing uid to int: %v", err)
        }

        gid, err := strconv.Atoi(els[2])
        if err != nil {
            return fmt.Errorf("error parsing gid to int: %v", err)
        }

        filename := els[3]

        ret = append(ret, Open{
            Pid:       int32(pid),
            Uid:       int32(uid),
            Gid:       int32(gid),
            Filename:  filename,
            Count:     int32(value),
        })
    }

    *d = ret

    // Assign the parsed data to the actual struct
    return nil
}

func (d *RecvData) UnmarshalJSON(b []byte) error {
    ret := RecvData{};

    // Create a temporary structure to match the JSON structure
    var temp Data;

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &temp); err != nil {
        return fmt.Errorf("error unmarshalling '%v': %v", string(b), err)
    }

    for key, interf := range temp.At {

        value, ok := interf.(float64)
        if !ok {
            return fmt.Errorf("error paring count. not an float64")
        }

        els := strings.Split(key, ",")

        if len(els) != 4 {
            return fmt.Errorf("error parsing kill data. key doesn't have 4 elements")
        }

        pid, err := strconv.Atoi(els[0])
        if err != nil {
            return fmt.Errorf("error parsing pid to int: %v", err)
        }

        uid, err := strconv.Atoi(els[1])
        if err != nil {
            return fmt.Errorf("error parsing uid to int: %v", err)
        }

        gid, err := strconv.Atoi(els[2])
        if err != nil {
            return fmt.Errorf("error parsing gid to int: %v", err)
        }

        fd, err := strconv.Atoi(els[3])
        if err != nil {
            return fmt.Errorf("error parsing fd to int: %v", err)
        }


        ret = append(ret, Recv{
            Pid:       int32(pid),
            Uid:       int32(uid),
            Gid:       int32(gid),
            Fd:        int32(fd),
            Count:     int32(value),
        })
    }

    *d = ret

    // Assign the parsed data to the actual struct
    return nil
}

func (d *SendData) UnmarshalJSON(b []byte) error {
    ret := SendData{};

    // Create a temporary structure to match the JSON structure
    var temp Data;

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &temp); err != nil {
        return fmt.Errorf("error unmarshalling '%v': %v", string(b), err)
    }

    for key, interf := range temp.At {

        value, ok := interf.(float64)
        if !ok {
            return fmt.Errorf("error paring count. not an float64")
        }

        els := strings.Split(key, ",")

        if len(els) != 5 {
            return fmt.Errorf("error parsing kill data. key doesn't have 4 elements")
        }

        pid, err := strconv.Atoi(els[0])
        if err != nil {
            return fmt.Errorf("error parsing pid to int: %v", err)
        }

        uid, err := strconv.Atoi(els[1])
        if err != nil {
            return fmt.Errorf("error parsing uid to int: %v", err)
        }

        gid, err := strconv.Atoi(els[2])
        if err != nil {
            return fmt.Errorf("error parsing gid to int: %v", err)
        }

        fd, err := strconv.Atoi(els[3])
        if err != nil {
            return fmt.Errorf("error parsing fd to int: %v", err)
        }

        length, err := strconv.Atoi(els[4])
        if err != nil {
            return fmt.Errorf("error parsing fd to int: %v", err)
        }


        ret = append(ret, Send{
            Pid:       int32(pid),
            Uid:       int32(uid),
            Gid:       int32(gid),
            Fd:        int32(fd),
            Len:       int32(length),
            Count:     int32(value),
        })
    }

    *d = ret

    // Assign the parsed data to the actual struct
    return nil
}

func (d *SocketData) UnmarshalJSON(b []byte) error {
    ret := SocketData{};

    // Create a temporary structure to match the JSON structure
    var temp Data;

    // Unmarshal into the temporary structure
    if err := json.Unmarshal(b, &temp); err != nil {
        return fmt.Errorf("error unmarshalling '%v': %v", string(b), err)
    }

    for key, interf := range temp.At {

        value, ok := interf.(float64)
        if !ok {
            return fmt.Errorf("error paring count. not an float64")
        }

        els := strings.Split(key, ",")

        if len(els) != 6 {
            return fmt.Errorf("error parsing kill data. key doesn't have 4 elements")
        }

        pid, err := strconv.Atoi(els[0])
        if err != nil {
            return fmt.Errorf("error parsing pid to int: %v", err)
        }

        uid, err := strconv.Atoi(els[1])
        if err != nil {
            return fmt.Errorf("error parsing uid to int: %v", err)
        }

        gid, err := strconv.Atoi(els[2])
        if err != nil {
            return fmt.Errorf("error parsing gid to int: %v", err)
        }

        family, err := strconv.Atoi(els[3])
        if err != nil {
            return fmt.Errorf("error parsing fd to int: %v", err)
        }

        sock_type, err := strconv.Atoi(els[4])
        if err != nil {
            return fmt.Errorf("error parsing fd to int: %v", err)
        }

        protocol, err := strconv.Atoi(els[5])
        if err != nil {
            return fmt.Errorf("error parsing fd to int: %v", err)
        }

        ret = append(ret, Socket{
            Pid:       int32(pid),
            Uid:       int32(uid),
            Gid:       int32(gid),
            Family:    int32(family),
            Type:      int32(sock_type),
            Protocol:  int32(protocol),
            Count:     int32(value),
        })
    }

    *d = ret

    // Assign the parsed data to the actual struct
    return nil
}
