# Fusion Core

**These build processes assume that you are executing make commands from the FusionCore 
directory**

**This guide also assumes you have install all the dependencies with `./dep.sh build`**

The Fusion Core is made up of 3 components

1) An ingest server, which sanitizes the data and stores it to the appropriate database
2) An influx container, which stores the time series data
3) A postgres container, which stores large file / byte data

The FusionCore requires 2 databases and can run as either a binary or a docker container. Because 
of this, the FusionCore has a lot of different build options, depending on how you'd like to 
deploy it.

To build the binary, the postgres database, the influx database, and set up the local file system, 
run:

```
make all
```

If you'd like more understanding of the build process, what you can build, etc. keep reading this 
document.

<br />

## Build the Databases

The first thing you'll need to build are the database images. This can be easily accomplished by
running:

```
make docker/postgres
make docker/influx
```

You can run these images with:

```
docker run -p 5432:5432 discernpsql:latest
docker run -p 8086:8086 discerninflux:latest
```

These images are built using the configuration file `FusionCore/dist/conf/CoreConfig.yaml`. The 
environment variables are sourced from this file and passed into the docker build environment.

The postgres image is named `discernpsql` and the FusionCore expects that name, if you have it 
explicitly control the container lifecycle.

The influx image is named `discerninflux` and the FusionCore expects that name, if you have it 
explicitly control the container lifecycle.

If `startnewinfluxinstance` or `startnewpostgresinstance` are set to true, the FusionCore can 
automatically stand up the databases. But this is not always adventageous. So, we 
developed a couple utilities to manually stand up the postgres and influx docker containers using a local 
copy of `/etc/discern/CoreConfig.yaml`. 

These can be built using:

```
make build/postgres
make build/influx
```

and run with:

```
./build/postgres
./build/influx
```

If you set `startnewinfluxinstance` and `startnewpostgresinstance` to `false`, these programs 
become very useful ways for you to check on the state of the databases. These programs will 
connect to them and perform a basic read. If any of that fails, the programs will explicitly let 
you know.

<br />

## Building the Fusion Core Server


Whether you are running FusionCore as a binary or a docker container, you need to build the actual
binary first. You can do this with:

```
make FusionCore
```

Before running the binary on the local machine, you need to install the system files, which can easily be done with:

```
make install
```

More details on the install process can be found [here](#installing-the-fusion-core-locally)

You can run the binary with:

```
./build/FusionCore
```

This will read `/etc/discern/CoreConfig.yaml` and set up the FusionCore and database images as you
specify.

To build the FusionCore into a docker container, run:

```
make docker/FusionCore
```

You can run this image with:

```
docker run -p 8086:8086 fusioncore:latest
```

This is going to place the FusionCore in a ubuntu container with `/etc/discern/CoreConfig.yaml` 
set to `FusionCore/dist/conf/CoreConfigNoDb.yaml`. **The docker version of the FusionCore
should not stand up any databases.** Instead, you should stand them up yourself and make sure 
`FusionCore/dist/conf/CoreConfigNoDb.yaml` has the IP addresses and ports of your influx and 
postgres instances.

<br />

## Building the Sandbox Test server

There is a sample environment provided by running:

```
make testing
./Tests/sandbox-topology/run
```

**in the root directory**

The example FusionCore that gets built is a docker container using 
`FusionCore/dist/conf/SandboxConf.yaml` as its `/etc/discern/CoreConfig.yaml`. This version uses 
the docker network addresses specified in `Tests/sandbox-topology/run`. 

Building all the testing pieces can be very slow, so if you only need to rebuild the FusionCore, 
run:

```
make docker/Sandbox
```

**in the FusionCore directory**

<br />

## Building the Debian

We also offer a debian installer of the FusionCore. To build it, run:

```
make deb/FusionCore
```

To install from the debian, simply run:

```
sudo dpkg -i ./FusionCore.deb
```

<br />

## Installing the Fusion Core Locally

The docker containers are conventient because they automatically set up the file system for you. 
But you might want to run the FusionCore on a dedicated machine, so to set up the file system we
have:

```
make install
```

Which is a handy wrapper around:

```
make install-directories 
make install-fusioncore 
make install-fusioncore-service 
make install-fusioncore-config 
```

`make install-directoriese` makes `/etc/discern`, `/etc/discern/postgres`, 
`/etc/discern/postgres/schema`

`make install-fusioncore` installs `FusionCore/build/FusionCore` to `/usr/local/bin/FusionCore`, 
which is where the system service (in `FusionCore/dist/systemd/FusionCore.service`) expects it.

`make install-fusioncore-service` installs `FusionCore/dist/systemd/FusionCore.service` into 
`/etc/systemd/system/FusionCore.service`

`make install-fusioncore-config` installs `FusionCore/dist/conf/CoreConfig.yaml` into 
`/etc/discer/CoreConfig.yaml`

Not all systems need all of these pieces, so they've been separated out so you can take what you 
need, if you need the fine-grained control.

<br />

## Un-installing a Local FusionCore

We have a handy wrapper to un-install all the pieces of installed by `make install`. 

Unsurprisingly, its:

```
make uninstall
```

This won't, however, completely delete `/etc/discern`, because you might have DataSorcerers 
running on the same machine.

<br />

## Cleaning

```
make clean
```

removes FusionCore/build, the fusioncore docker image, the sandbox image, the discernpsql image, 
and the discerninflux image.

