package main;

import (
    "time"
    "github.com/shirou/gopsutil/cpu"
)

func getCPULoad() ([]float64, error) {
    percent, err := cpu.Percent(time.Second, true) // true means give per cpu data
    if err != nil {
        return []float64{}, err
    }
    return percent, err
}

