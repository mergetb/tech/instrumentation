package file;

import (
    "time"
    "sync"
    "context"
    "strconv"
    log "github.com/sirupsen/logrus"
    "github.com/influxdata/influxdb-client-go/v2"
    "github.com/influxdata/influxdb-client-go/v2/api"

    bridge "FusionBridge/metadata/file"

    config "gitlab.com/instrumentation/FusionCore/pkg/config"
)


type FileServer struct {
    bridge.UnimplementedFileServer
    mu sync.Mutex
    writeAPI api.WriteAPIBlocking
    queryAPI api.QueryAPI
}


func NewServer(client influxdb2.Client) *FileServer {
    s := &FileServer{
        writeAPI: client.WriteAPIBlocking(config.Settings.ORG, config.Settings.BUCKET_NAME),
        queryAPI: client.QueryAPI(config.Settings.ORG),
    }
    return s
}


func (s *FileServer) SaveFsEvent(ctx context.Context, 
    MSG *bridge.FsEvent) (*bridge.FsEventACK, error) {

    // Write the read data to the writeAPI
    tags := map[string]string{
            "DevID":MSG.DevID,
            "Owner":MSG.Owner,
            "Location":MSG.Location,
            "Op": strconv.Itoa(int(MSG.Op)),
    }
    // Content is optional but protobufs have auto empty values so ok 
    fields := map[string]interface{}{
            "Content":MSG.Content,
            "Permissions":MSG.Permissions,
            "Group":MSG.Group,
    }

    ctx, cancel := context.WithTimeout(context.Background(), 
        10*time.Second)
    defer cancel()


    point := influxdb2.NewPoint("file", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(ctx, point)
    if err != nil {
        log.Errorf("error in file.SaveFsEvent: %v", err)
        return &bridge.FsEventACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }

    // Could add more error codes for client health and such
    //     Will definitiely add error for write issues
    return &bridge.FsEventACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}

