# Saved Network Data

The kind of data saved from network activity largely depends on the kinds of packets being sent, 
but there is some common data. 

The common data is listed at the top and all network entries will have these fields (even if they 
are empty). The rest of the document lists what kinds of data is stored for the different kinds of
packets.

All the data is stored in the influx database as "network" measurements. We DON'T record 
different measurements for different packet types.


## Common Data:

Tags:

- DevID
- LinkProtocol
- NetworkProtocol
- TransportProtocol
- ApplicationProtocol

Fields:

- Dev: The interface the packet was sent on


## ARP Packets

Tags:

- SrcHwAddy: The Senders Hardware Address
- SrcProtAdd: The Senders Protocol Address
- DstHwAddy: The Target Hardware Address
- DstProtAdd: The Target Protocol Address

Fields:

- Operation: The Arp operation (request or reply)
- Protocol: The protocol type
 

## Ethernet Packet

Tags:

- SRC_MAC: The source's MAC address
- DST_MAC: The destination's MAC address

Fields:

- Length: The length of the packet


## IP Packets

Tags:

- SRC_IP: The source's IP address
- DST_IP: The destination's IP address

Fields:

- Version: Either "v4" or "v6"


## TCP or UDP

Tags:
- DstPort: Specifies the destination port

Fields:
- SrcPort: Specifies the source port


## DNS

Fields:
- Questions: A string of all the DNS questions
- Answers: A string of the DNS answers

