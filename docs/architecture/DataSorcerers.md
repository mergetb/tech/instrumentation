# Global Architecture

All the binaries are stored in `/usr/local/bin` with the format: discern-\<sorcerername>-sorcerer, with the single exception of `/usr/local/src/start_ttylog.sh`, which is used to record bash inputs. 

All services have the same format: discern-\<sorcerername>-sorcerer, so you
    can grep for them quite easily

Every service uses the same config file: `/etc/discern/SorcererConfig.yaml`

An example config can be found at DataSorcerers/dist/conf/SorcererConfig.yaml

Every services logs its information to stdio and to the same file, specified by the config 
parameter `logfile`. Each program adds a timestamp and its name to the log submission, so they 
are easy to parse. This file defaults to the same file the FusionCore server writes to.

To avoid spamming the network, some sorcerers dump their data in intervals, instead of as soon as 
they generate it. Check the config file in options (`docs/conf`) to see which Sorcerers are on a 
timer.

<br/>

# Control Sorcerers Architecture

There are 3 Control Data Sorcerers which scrape data for ansible configs / playboos, bash commands entered, 
    and jupyter notebooks respectively 

**Each of these services can be run independently and do not require
        the others to run**

The Ansible and Jupyter sorcerer should be run on any control nodes

The Bash sorcerer should be run locally on any node whos bash interactions you
    want to record

<br />

## Ansible Implementation

This service periodically scans the system for ansible config files and playbooks

Ansible config files can be found at $ANSIBLE_CONFIG, ~/.ansible.cfg,
        ansible.cfg (in current directory), or /etc/ansible/ansible.cfg

Ansible playbooks can be identified by searching for yaml files containing
        the line `- hosts:`


## Bash Implementation

This service runs strace to listen for prints to STDIO and keyboard 
    entries. These outputs are saved, with timetamps, files in 
    /var/log/ttylog/

This program is split into 2 separate services. The first listens for
    user IO and records the text and timestamps to a file. The second 
    reads this file and dumps it to the Fusion Core.

The output files are then collected and periodically dumped to the Core
    server.


## Jupyter Implementation

Jupyter notebooks can be identified with the file extension .ipynb. The
    jupyter scraper simply scans directories with this file extension and 
    saves them to the database

Scraping every directory in the system is quite a large task, so the search space is reduced to 
recursively searching the directories specified in `/etc/discern/SorcerersConfig.yaml`. See 
`docs/conf/DataSorcerers` for more detail on how you can specify the path

<br />
<br />

# Log Sorcerer Architecture

This service captures dmesg logs and any other log files you specify. This recording is done 
periodically and only records the last X lines of each log file. See `docs/conf/DataSorceres` to 
see how to specify the log files swept, the interval they are swept, and how many lines are recorded.

<br />
<br />

# Metadata Sorcerers Architecture

Each of these services are implemented independently and do not require
    the others to run

<br />

## File Metadata Sorcerer

This service uses fsnotify (an inotify wrapper in golang) to trigger 
    data recording events on file creation, deletion, renaming, writing, 
    and permissions changing.

The files which this programs listen to can be changed using the 
    config option: startingdirs. These directories are recursively 
    iterated down and listeners are added to all files and directories.

Certain folders can generate lots of data, so a blacklist is also 
    included to prevent the program from listening to certain files 
    and folders. This can be specified with: blacklistdirs.


### When & What Data is Recorded: 

When a file is writen to, the binary data of the updated file is saved 
    to the database. 

When a file is created or has permissions changed, the owner and group
    is saved to the database.

When a file has its name changed, the new name is recorded to the 
    database

When a file is deleted only a timestamp is recorded



## ID Metadata Sorcerer

This service scrapes 2 kinds of information, users on the machine and
    interface information. The user information is scraped from `/home`.


### Interface information

The information for each interface is:

- MAC address
- Name
- An array of hostname and IP pairs

This should allow for correlation between the packets data and the 
    device which sent the data, even in the event of changing IPs



## Network Metadata Sorcerer

This service sets a listener on every interface accessible via 
    gopacket/pcap and sends detailed information on the packet:

For ARP Packets:

- Source Hardware Address
- Source Protocol Address
- Destination Hardware Address
- Destination Protocol Address
- ARP Operation Code
- ARP Protocol

For Ethernet Packets:

- Source MAC Address
- Destination MAC Address
- Payload length

For IP Packets:

- Source IP Address
- Destination IP Address
- V4 / V6 boolean

For TCP Packets:

- Source Port
- Destination Port

For UDP Packets:

- Srouce Port
- Destination Port

For DNS Packets:

- DNS Questions
- DNS Resource Records

TLS and ICMP application layer protocols are also saved

The device the data was sent on


## OS Metadata Sorcerer

The OS metadata is separated into two sections. The first section is a 
    series of bpftrace programs which log data, as jsons, into files 
    in /tmp/discern/data/os/. The second part is a scrapper, written in 
    go, which parses the json and sends the data, periodically, to the
    Core data server. Each bpftrace program has a different json output,
    so we cannot generalize the client to allow for an arbitrary bpftrce
    program. Thankfully, implementing new bpftrace prgrams is rather 
    trivial.

To kill the data recording program, you will need to kill the series of
    bpftrace programs which spawn when the service is started. The PIDs
    can easily be found with : `sudo ps -e | grep bpftrace`

The actual sorcerer implementation is a nice little parser with a save function after the 
equivalent of a lexing step. Very fun program to write and dead simple to extend.


A following is a list of all the systemcalls being recorded and the
    data recorded along side the system call:

- close: # of times called by a give (pid, uid, gid) tuple over a 
        60 second interval

- closerange: # of times called by a give (pid, uid, gid) pair 
        over a 60 second interval

- execve: timestamp, pid, uid, gid, and args0-4. Because kprobs 
        don't allow for looping, reading all the arguments is hard. 4
        seemed like a sweet spot to get all the necessary information

- execveat: timestamp, pid, uid, gid, and args0-4. Because kprobs 
        don't allow for looping, reading all the arguments is hard. 4
        seemed like a sweet spot to get all the necessary information

- fork: Counts the number of times a given (pid, tid, uid, gid) 
        tuple calls fork

- kill: Records the signal and program PID a given (pid, uid, gid)
        called kill on

- open: Conuts the number of times a given (pid, uid, gid) opens
        a particular file 

- openat: Conuts the number of times a given (pid, uid, gid) opens
        a particular file 

- recvfrom: Counts the number of times a given (pid, uid gid)
        recieves from a given file descriptor

- recvmmsg: Counts the number of times a given (pid, uid gid)
        recieves from a given file descriptor

- recvmsg: Counts the number of times a given (pid, uid gid)
        recieves from a given file descriptor

- sendmmsg: Counts the number of times a given (pid, uid gid)
        recieves from a given file descriptor and the length

- sendmsg: Counts the number of times a given (pid, uid gid)
        recieves from a given file descriptor and the length

- sendto: Counts the number of times a given (pid, uid gid)
        recieves from a given file descriptor and the length

- socket: Records how many sockets with a given family, type, 
        and protocol a (pid, uid, gid) creates

- socketpair: Records how many sockets with a given family, type, 
        and protocol a (pid, uid, gid) creates

- socketpair: Records how many sockets with a given family, type, 
        and protocol a (pid, uid, gid) creates

- sysinfo: Records when a given (pid, uid, gid) calls to ask for 
        system information

- tkill: Counts how many times a given (pid, uid, gid) calls tkill
        as well as the signal and argument pid that it gets called on

- vfork: Counts how many time a given (pid, uid, gid) calls vfork

<br />

# Validation Sorcerers Architecture

This is just a simple test you can run on a client to verify the setup is working properly. It 
doesn't get installed on the system and should just be run as a binary.

