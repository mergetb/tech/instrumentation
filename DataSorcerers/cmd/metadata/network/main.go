package main;

import (
    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)

const (
    snapLen = 1024
)


func main() {
    // Load config options
    config.LoadConfig()
    if !config.Settings.RunNetwork { return }

    err := log.InitializeLogOutput("Network")
    if err != nil {
        log.Fatalf("error initializing log output: %v", err)
    }

    manager := DataManager{}
    err = manager.ConnectToFusionCore()
    if err != nil {
        log.Fatalf("failed to connect to fusion core: %v", err)
    }

    err = manager.LoadSettings() 
    if err != nil {
        log.Fatalf("failed to load settings into data manager: %v", err)
    }

    if config.Settings.LogToIntermediateFile {
        err = helpers.BuildDir(config.Settings.IntermediateDataFolder)
        if err != nil {
            log.Fatalf("error building intermediate data dir: %v", err)
        }
    }
    
    err = manager.FindAllDevices()
    if err != nil {
        log.Fatalf("failed to find network devices: %v", err)
    }


    err = manager.Listen()
    if err != nil {
        log.Fatal("failed listening to devices: %v", err)
    }
}

