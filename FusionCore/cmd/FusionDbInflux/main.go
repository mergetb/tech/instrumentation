package main

import (
    "time"
    log "github.com/sirupsen/logrus"
    "github.com/influxdata/influxdb-client-go/v2"

    "gitlab.com/instrumentation/FusionCore/pkg/config"
    "gitlab.com/instrumentation/FusionCore/pkg/databases/influx"
)

func main() {
    err := config.LoadConfig()
    if err != nil {
        log.Fatalf("error loading fusioncore config: %v", err)
    }

    err = influx.StartDB()
    if err != nil {
        log.Fatalf("error starting influx db: %v", err)
    }

    var client influxdb2.Client

    for client, err = influx.GenerateClient(); err != nil; client, err = influx.GenerateClient() {
        log.Info("failed to generate influx client. pausing then trying again")
        time.Sleep(1 * time.Second)
    }

    err = influx.VerifyConnection(client)
    if err != nil {
        log.Fatalf("failed to verify influx client connection: %v", err)
    }

    log.Info("started influx container and health checks passed")
}

