package cpu; 


import (
    "time"
    "sync"
    "context"
    "strconv"
    log "github.com/sirupsen/logrus"
    "github.com/influxdata/influxdb-client-go/v2"
    "github.com/influxdata/influxdb-client-go/v2/api"

    bridge "FusionBridge/metadata/cpu"

    config "gitlab.com/instrumentation/FusionCore/pkg/config"
)


type CpuServer struct {
    bridge.UnimplementedCpuServer
    mu sync.Mutex
    writeAPI api.WriteAPIBlocking
    queryAPI api.QueryAPI
}


func NewServer(client influxdb2.Client) *CpuServer {
    s := &CpuServer{
        writeAPI: client.WriteAPIBlocking(config.Settings.ORG, config.Settings.BUCKET_NAME),
        queryAPI: client.QueryAPI(config.Settings.ORG),
    }
    return s
}


func (s *CpuServer) SaveCpuLoad(ctx context.Context, 
    MSG *bridge.CpuLoad) (*bridge.CpuACK, error) {

    for cpu, load := range MSG.Load {

        // Write the read data to the writeAPI
        tags := map[string]string{
                "DevID":MSG.DevID,
                "Cpu": strconv.Itoa(cpu),
        }
        // Content is optional but protobufs have auto empty values so ok 
        fields := map[string]interface{}{
                "Load":load,
        }

        ctx, cancel := context.WithTimeout(context.Background(), 
            10*time.Second)
        defer cancel()

        point := influxdb2.NewPoint("cpu-load", tags, fields, 
            MSG.TimeStamp.AsTime())
        
        err := s.writeAPI.WritePoint(ctx, point)
        if err != nil {
            log.Errorf("error in file.SaveFsEvent: %v", err)
            return &bridge.CpuACK{
                Type: 1, 
                SubmissionNumber: MSG.SubmissionNumber,
            }, err
        }
    }

    return &bridge.CpuACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}

