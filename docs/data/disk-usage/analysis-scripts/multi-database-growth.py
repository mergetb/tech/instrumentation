#!/bin/python3

import matplotlib.pyplot as plt


psql_data = [ ]
influx_data = [ ]

# file = './data/mergetb/baselines/os.txt'
file = './data/mergetb/baselines/logs.txt'

print(f"Reading {file}")


with open(file, 'r') as fd:
    for line in fd:
        line = line.strip()
        if line[-1] == ",":
            line = line[:-1]
    
        if len(line.split(": ")) > 1:
            [ database, value ] = line.split(": ")
            
            if database == "postgres":
                psql_data += [ int(value) ]
            elif database == "influx":
                influx_data += [ int(value) ]
            else:
                raise Exception(f"database type not postgres/influx. Instead, its: {database}")

            continue

        influx_data += [ int(line) ]


# # Find slope of the data. Take in half second intervals, so .5x # of data points is the time
start = 50
stop = 250
influx_slope = (influx_data[stop] - influx_data[start]) / ((stop - start) / 2)
postgres_slope = (psql_data[stop] - psql_data[start]) / ((stop - start) / 2)
print(f"Total Slope: {influx_slope} (influx) + {postgres_slope} (psql) = {influx_slope + postgres_slope}")


# Create a line plot
plt.figure(figsize=(10, 6))
plt.plot(influx_data, marker='o', linestyle='-', label='Data Points')
plt.title('Influx Data Trend')
plt.xlabel('Index')
plt.ylabel('Value')
plt.grid(True)
plt.legend()
plt.show()


# Create a line plot
plt.figure(figsize=(10, 6))
plt.plot(psql_data, marker='o', linestyle='-', label='Data Points')
plt.title('Postgres Data Trend')
plt.xlabel('Index')
plt.ylabel('Value')
plt.grid(True)
plt.legend()
plt.show()


