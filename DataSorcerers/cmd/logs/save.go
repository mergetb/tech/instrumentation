package main;

import (
    "fmt"
    "time"
    "bytes";
    "context"
    "strings"
    "os/exec";
    "io/ioutil"
    "google.golang.org/grpc" 
    "github.com/golang/protobuf/ptypes"

    bridge "FusionBridge/log"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


var opts []grpc.DialOption;
var conn *grpc.ClientConn;
var client bridge.LogClient;

func ConnectToFusionCore() error {
    conn = helpers.CreateConnection()
    client = bridge.NewLogClient(conn); 
 
    return nil
}

// Async this so we can process faster
func SaveLog(full_path string, log_lengths *map[string]int) error {
    ctx, cancel := context.WithTimeout(context.Background(), 
        10*time.Second)
    defer cancel()

    data, err := ioutil.ReadFile(full_path) 
    if err != nil {
        return fmt.Errorf("error reading %v: %v", full_path, err);
    }

    lines := strings.Split(string(data), "\n")

    lc := len(lines)

    // If there aren't any new lines added
    prev_lc, exists := (*log_lengths)[full_path];
    if  exists && prev_lc == lc {
        return nil
    }

    (*log_lengths)[full_path] = lc

    // Record either the newest lines or the config.Settings.LogLinesCaptured newest lines, if 
    //   there are a lot of logs added
    delta := config.Settings.LogLinesCaptured
    if exists && lc - prev_lc < config.Settings.LogLinesCaptured {
        delta = lc - prev_lc
    }

    index := len(lines) - delta
    if len(lines) < delta { 
        index = 0;
    }

    timestamp, err := ptypes.TimestampProto(time.Now())
    if err != nil {
        return fmt.Errorf("error creating timestamp: %v", err)
    }

    MSG := bridge.LogData{
        SubmissionNumber : 0,
        TimeStamp : timestamp,
        Location: full_path, 
        Content: strings.Join(lines[index:], "\n"),
        DevID: config.GetID(),
    }

    e := helpers.SaveToIntermediate(&MSG, "logs")
    if e != nil {
        log.Errorf("error saving log data to file: %v", e)
    }

    _, e = client.SaveLog(ctx, &MSG)
    if e != nil {
        return fmt.Errorf("client error saving log to fusion core: %v", e)
    }

    return nil
}

func SaveDmesg(log_lengths *map[string]int) error {
    // Try to read dmesg file
    err := SaveLog("/var/log/dmesg", log_lengths)
    if err == nil { return nil }

    log.Info("/var/log/dmesg not found. trying to capture from cli")

    cmd := exec.Command("dmesg")
    var outb, errb bytes.Buffer
    cmd.Stdout = &outb
    cmd.Stderr = &errb
    err = cmd.Run()

    // If there is an error then kill
    if len(errb.String()) != 0 {
        return fmt.Errorf("error reading dmesg from cli: %v", err)
    }

    dmesgOutputArray := strings.Split(outb.String(), "\n")

    lc := len(dmesgOutputArray)
    prev_lc, exists := (*log_lengths)["dmesg"];
    // If there aren't any new lines, don't write a new message
    if  exists && prev_lc == lc {
        return nil
    }

    (*log_lengths)["dmesg"] = lc

    delta := config.Settings.LogLinesCaptured
    if exists && lc - prev_lc < config.Settings.LogLinesCaptured {
        delta = lc - prev_lc
    }

    startIndex := len(dmesgOutputArray) - delta

    linesToSave := dmesgOutputArray[startIndex:]

    timestamp, err := ptypes.TimestampProto(time.Now())

    MSG := bridge.LogData{
        SubmissionNumber : 0,
        TimeStamp : timestamp,
        Location: "dmesg-cli", 
        Content: strings.Join(linesToSave, "\n"),
        DevID: config.GetID(),
    }

    e := helpers.SaveToIntermediate(&MSG, "logs")
    if e != nil {
        log.Errorf("error saving dmesg data to file: %v", e)
    }

    _, e = client.SaveLog(context.Background(), &MSG)
    if e != nil {
        log.Errorf("client error in Save Log Data: %v", e)
        return e
    }
    
    log.Info("captured dmesg from cli")

    return nil
}

