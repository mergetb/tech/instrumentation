package main;

import (
    "os"
    "fmt"
    "time"
    "strings"
    "context"
    "google.golang.org/grpc" 
    "github.com/golang/protobuf/ptypes"

    bridge "FusionBridge/control/bash"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


var opts []grpc.DialOption;
var conn *grpc.ClientConn;
var client bridge.BashClient;


func ConnectToFusionCore() error {
    conn := helpers.CreateConnection()
    client = bridge.NewBashClient(conn);

    return nil
}


func saveCsvInCore(csv string, host string, user string, cnt int) error {
    
    cmds, err :=  os.ReadFile(csv)
    if err != nil {
        // Could add remove file here, but other processes could be writing
        return err
    }

    // Pop an error so the log isn't emptied, in case the session needs to end
    if strings.TrimSpace(string(cmds)) == "" {
        return fmt.Errorf("csv not built. either session hasn't ended or no commands executed. plz don't panic")
    }
  
    ctx, cancel := context.WithTimeout(context.Background(), 
        10*time.Second)
    defer cancel()

    timestamp, err := ptypes.TimestampProto(time.Now())
    if err != nil {
        return err
    }

    MSG := bridge.CmdSnapShot{
        SubmissionNumber : 0,
        TimeStamp : timestamp,
        Cmds : string(cmds), 
        Host: host,
        Count: int32(cnt),
        User: user,
        DevID: config.GetID(),
    }

    e := helpers.SaveToIntermediate(&MSG, "bash")
    if err != nil {
        log.Errorf("error logging bash cmds to file: %v", err)
    }

    _, e = client.IngestCmdSnapShot(ctx, &MSG)
    if e != nil {
        return e
    }

    return nil
}

