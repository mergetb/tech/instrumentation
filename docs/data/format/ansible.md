# Saved Ansible Data

The Data Sorcerer records 2 kinds of Ansible data:

1) The Ansible configuration
2) The actual Ansible playbook


## Ansible Configuration Data

The Ansible config file is stored in the postgres database. Its schema can be found at:
`FusionCore/dist/docker/postgres/schema/ansible.sql`. 

The content of the config is hashed, to be used as a foreign key with the influx database. 

FusionCore then stores the Device ID, location of the file, and the file's hash to the influx 
database as an "ansible-config" measurement.

`DevID` & `Location` are tags

`ContentHash` is a field


## Ansible Playbook

Playbook data is saved into postgres. Its schema can be found at 
`FusionCore/dist/docker/postgres/schema/ansible.sql`

The content is hashed to use as a foreign key with the influx database.

FusionCore then stores the Device ID, the location of the file, the file's hash, and a boolean
representing whether this is a "verified" Ansible playbook to the Influx database as an "ansible-playbooks" measurement.

`DevID` & `Location` are tags

`ContentHash` & `Verified` are a fields

Verifying a playbook currently consists of finding a host field in the file. If you'd like more 
detail than that, update the sorcerer.

