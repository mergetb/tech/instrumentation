package main

import (
    "net"
    "time"
    "strconv"

    "google.golang.org/grpc"

    log "github.com/sirupsen/logrus"

    "github.com/influxdata/influxdb-client-go/v2"

    config "gitlab.com/instrumentation/FusionCore/pkg/config"
    s_influx "gitlab.com/instrumentation/FusionCore/pkg/databases/influx"
    s_postgres "gitlab.com/instrumentation/FusionCore/pkg/databases/postgres"

    b_validation "FusionBridge/validation"
    s_validation "gitlab.com/instrumentation/FusionCore/pkg/validation"

    b_jupyter "FusionBridge/control/jupyter"
    s_jupyter "gitlab.com/instrumentation/FusionCore/pkg/ingest/control/jupyter"

    b_id "FusionBridge/metadata/id"
    s_id "gitlab.com/instrumentation/FusionCore/pkg/ingest/metadata/id"

    b_log "FusionBridge/log"
    s_log "gitlab.com/instrumentation/FusionCore/pkg/ingest/log"

    b_ansible "FusionBridge/control/ansible"
    s_ansible "gitlab.com/instrumentation/FusionCore/pkg/ingest/control/ansible"

    b_bash "FusionBridge/control/bash"
    s_bash "gitlab.com/instrumentation/FusionCore/pkg/ingest/control/bash"

    b_cpu_meta "FusionBridge/metadata/cpu"
    s_cpu_meta "gitlab.com/instrumentation/FusionCore/pkg/ingest/metadata/cpu"

    b_file_meta "FusionBridge/metadata/file"
    s_file_meta "gitlab.com/instrumentation/FusionCore/pkg/ingest/metadata/file"

    b_network_meta "FusionBridge/metadata/network"
    s_network_meta "gitlab.com/instrumentation/FusionCore/pkg/ingest/metadata/network"

    b_os_meta "FusionBridge/metadata/os"
    s_os_meta "gitlab.com/instrumentation/FusionCore/pkg/ingest/metadata/os"
)

func main() {

	log.Info("loading config")

        err := config.LoadConfig()
        if err != nil {
            log.Fatalf("error loading fusioncore config: %v", err)
        }

	log.Info("server booting")

	// Setting up all the Server listening stuff
        addr := config.Settings.ListenIP + ":" + strconv.Itoa(int(config.Settings.GRPC_Port))
	lis, err := net.Listen(config.Settings.ListenProto, addr)
	if err != nil {
		log.Fatalf("grpc listen failed with error: %v", err)
	}
	var opts = []grpc.ServerOption{
		grpc.MaxRecvMsgSize(10 * 1024 * 1024),
	}
	grpcServer := grpc.NewServer(opts...)


	// Setting up the database connection (bound to services through
	//     the NewServer handler)
        err = s_postgres.StartDB()
        if err != nil {
            log.Fatalf("couldn't start postgres: %v", err)
        }

	err = s_postgres.Connect()
        if err != nil {
            log.Fatalf("unable to connect to postgres: %v", err)
        }

        for tmp := s_postgres.VerifyConnection(); tmp != nil; tmp = s_postgres.VerifyConnection() {
            log.Info("failed to verify connection to postgres. pausing then retrying")
            time.Sleep(2 * time.Second)
        }



        err = s_influx.StartDB()
        if err != nil {
            log.Fatalf("couldn't start influx: %v", err)
        }

        var client influxdb2.Client
        for client, err = s_influx.GenerateClient(); err != nil; client, err = s_influx.GenerateClient() {
            log.Info("failed to generate influx client. pausing then trying again")
            time.Sleep(1 * time.Second)
        }

        err = s_influx.VerifyConnection(client)
        if err != nil {
            log.Fatalf("failed to verify influx client connection: %v", err)
        }

	// REGISTER ALL OF YOUR SERVICES HERE

	// Basic check to see that the server is generally working
	b_validation.RegisterSendAndRecvServer(grpcServer,
		s_validation.NewServer())
	// End points to recieve all the juypter related information
	b_jupyter.RegisterJupyterServer(grpcServer,
		s_jupyter.NewServer(client))
	// Register the Bash server
	b_bash.RegisterBashServer(grpcServer,
		s_bash.NewServer(client))
	// Register the Ansible server
	b_ansible.RegisterAnsibleServer(grpcServer,
		s_ansible.NewServer(client))
	// Register the Log server
	b_log.RegisterLogServer(grpcServer,
		s_log.NewServer(client))
	// Register the File Metadata Server
	b_cpu_meta.RegisterCpuServer(grpcServer,
		s_cpu_meta.NewServer(client))
	// Register the File Metadata Server
	b_file_meta.RegisterFileServer(grpcServer,
		s_file_meta.NewServer(client))
	// Register the Network metadata server
	b_network_meta.RegisterNetworkServer(grpcServer,
		s_network_meta.NewServer(client))
	// Register the OS metadata server
	b_os_meta.RegisterOSServer(grpcServer,
		s_os_meta.NewServer(client))
	// Register the ID metadata server
	b_id.RegisterIDServer(grpcServer,
		s_id.NewServer(client))

	log.Info("finished registering all services")

	log.Info("running serving")
	grpcServer.Serve(lis)
}

