package bash;

import (
    "fmt"
    "sync"
    "strconv"
    "strings"
    "context"
    log "github.com/sirupsen/logrus"
    "github.com/influxdata/influxdb-client-go/v2"
    "github.com/influxdata/influxdb-client-go/v2/api"

    bridge "FusionBridge/control/bash"

    config "gitlab.com/instrumentation/FusionCore/pkg/config"
)


type BashServer struct {
    bridge.UnimplementedBashServer
    mu sync.Mutex
    writeAPI api.WriteAPIBlocking
    queryAPI api.QueryAPI
}


func NewServer(client influxdb2.Client) *BashServer {
    s := &BashServer{
        writeAPI: client.WriteAPIBlocking(config.Settings.ORG, config.Settings.BUCKET_NAME),
        queryAPI: client.QueryAPI(config.Settings.ORG),
    }
    return s
}


func (s *BashServer) IngestCmdSnapShot(ctx context.Context, 
    MSG *bridge.CmdSnapShot) (*bridge.BashACK, error) {

    if strings.TrimSpace(MSG.Cmds) == "" {
        return &bridge.BashACK{
            Type: 0, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, nil
    }


    // Write the read data to the writeAPI
    tags := map[string]string{
            "DevID":MSG.DevID,
            "User":MSG.User,
            "Host": MSG.Host,
            "Count":strconv.Itoa(int(MSG.Count)),
    }
    fields := map[string]interface{}{
            "Cmds":fmt.Sprintf("%v", MSG.Cmds),
    }

    point := influxdb2.NewPoint("bash", tags, fields, 
        MSG.TimeStamp.AsTime())
    
    err := s.writeAPI.WritePoint(context.Background(), point)
    if err != nil {
        log.Info(fmt.Sprintf("Error in bash.IngestCmdSnapShot: %v", err))
        return &bridge.BashACK{
            Type: 1, 
            SubmissionNumber: MSG.SubmissionNumber,
        }, err
    }

    // Could add more error codes for client health and such
    //     Will definitiely add error for write issues
    return &bridge.BashACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}

