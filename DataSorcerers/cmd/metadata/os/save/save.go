package save;

import (
    "os"
    "fmt"
    "bufio"
    "strings"
    "context"
    "encoding/json"
    "google.golang.org/grpc" 

    bridge "FusionBridge/metadata/os"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)

// Interface that all files we want to save with "OsFileSaver" must follow
// Also needs ability to properly parse JSON, but thats a runtime issue and shouldn't technically 
// be a problem since outputs are always valid jsons
type JsonData interface {
    SaveToFusionCore(bridge.OSClient) error
}


func OpenSyscallDataFile(syscall string) (*os.File, error) {
    file_loc := config.Settings.OsDataDir + "/" + syscall + "-res.txt"

    file, err := os.OpenFile(file_loc, os.O_RDWR|os.O_CREATE, 0666)
    if err != nil {
        return nil, err
    }

    return file, nil
}



type OsFileSaver struct {
    opts []grpc.DialOption;
    conn *grpc.ClientConn;
    client bridge.OSClient;
}


func (o *OsFileSaver) ConnectClient() {
    o.conn = helpers.CreateConnection()
    o.client = bridge.NewOSClient(o.conn); 
}


func (o *OsFileSaver) SaveData(syscall string) error {
    // Going to do best job with parsing. Save what we can del rest
    jsonData, err := o.ParseData(syscall)
    if err != nil {
        log.Errorf("error parsing %v's json data: %v", syscall, err)

        if strings.Contains(err.Error(), "timestamp but no data") {
            deleteSavedSyscallData(syscall)
            return nil
        } 
    }

    if jsonData == nil {
        return fmt.Errorf("error building jsonData for %v: %v", syscall, err)
    }

    err = jsonData.SaveToFusionCore(o.client)
    if err != nil {
        return fmt.Errorf("error saving %v to fusioncore %v", syscall, err)
    }

    // Try deleting. If error, at least try to save the data
    err = deleteSavedSyscallData(syscall)
    if err != nil {
        log.Errorf("error removing saved data for %v syscall: %v", syscall, err)
    }

    return nil
}


// We use a basic parsing loop and the json library to deal with any mal-formatted docs
func (o *OsFileSaver) ParseData(syscall string) (JsonData, error) {
    finalJsonData, err := o.FileToDataStruct(syscall)
    if err != nil {
        return nil, err
    }
    // To hold the data parsed out of a single line, compared to whole file
    tmpJsonData, err := o.FileToDataStruct(syscall)
    if err != nil {
        return finalJsonData, err
    }

    file, err := OpenSyscallDataFile(syscall)
    if err != nil {
        return finalJsonData, fmt.Errorf("error opening data file %v for parsing: %v", syscall, err)
    }
    defer file.Close()

    timeData := &JsonTimeStamp{}

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        // Skip blanks
        line := strings.TrimSpace(scanner.Text())
        if len(line) == 0 { continue }

        // Unmarshal the timestamp for the data. If this fails, the next line is either blank or data
        for err = json.Unmarshal([]byte(line), timeData); err == nil; err = json.Unmarshal([]byte(line), timeData) { 
            // Read the next line
            if !scanner.Scan() {
                return finalJsonData, fmt.Errorf("file ended prematurely. timestamp but no data")
            }

            // Skip all blank lines
            for line = scanner.Text(); len(line) == 0 && scanner.Scan(); line = scanner.Text() {
                continue
            }
            // Update line just in case
            line = scanner.Text()

            if len(line) == 0 { 
                return finalJsonData, fmt.Errorf("file ended prematurely, only blank lines. quitting")
            }
        }

        // Unmarshal the actual os trigger data
        if err := json.Unmarshal([]byte(line), tmpJsonData); err != nil {
            return finalJsonData, fmt.Errorf("error unmarshaling tmpJsonData from file: %v", err)
        }
        // Save the data for export. Fun with interfaces
        finalJsonData, err = AppendData(finalJsonData, tmpJsonData, timeData)
        if err != nil {
            return finalJsonData, fmt.Errorf("error in AppendData: %v", err)
        }
    }
   
    return finalJsonData, nil
}


// For JSON un-marshaling and ParseData() purposes
func (o *OsFileSaver) FileToDataStruct(syscall string) (JsonData, error) {
    switch syscall {
    case "close":
        return &CloseRootJson{Range: false}, nil
    case "closerange":
        return &CloseRootJson{Range: true}, nil
    case "execve":
        return &ExecveRootJson{At: false}, nil
    case "execveat":
        return &ExecveRootJson{At: true}, nil
    case "sysinfo":
        return &SysInfoRootJson{}, nil
    case "fork":
        return &ForkRootJson{V: false}, nil
    case "vfork":
        return &ForkRootJson{V: true}, nil
    case "kill":
        return &KillRootJson{T: false}, nil
    case "tkill":
        return &KillRootJson{T: true}, nil
    case "open":
        return &OpenRootJson{Variation: ""}, nil
    case "openat":
        return &OpenRootJson{Variation: "At"}, nil
    case "openat2":
        return &OpenRootJson{Variation: "At2"}, nil
    case "recvfrom":
        return &RecvRootJson{Variation: "From"}, nil
    case "recvmmsg":
        return &RecvRootJson{Variation: "MMsg"}, nil
    case "recvmsg":
        return &RecvRootJson{Variation: "Msg"}, nil
    case "sendmmsg":
        return &SendRootJson{Variation: "MMsg"}, nil
    case "sendmsg":
        return &SendRootJson{Variation: "Msg"}, nil
    case "sendto":
        return &SendRootJson{Variation: "To"}, nil
    case "socket":
        return &SocketRootJson{Pair: false}, nil
    case "socketpair":
        return &SocketRootJson{Pair: true}, nil
    default: 
        return nil, fmt.Errorf("syscall doesn't exists with the filename: %v", syscall)
    }

    return nil, fmt.Errorf("syscall doesn't exists with the filename: %v", syscall)
}


func AppendData(final JsonData, tmp JsonData, time *JsonTimeStamp) (JsonData, error) {
    switch f := final.(type) {
    case *CloseRootJson:
        if t, ok := tmp.(*CloseRootJson); ok {
            f.Data = append(f.Data, t.Data...)
            f.TimeStamps = append(f.TimeStamps, time)
            return f, nil
        }
    case *ExecveRootJson:
        if t, ok := tmp.(*ExecveRootJson); ok {
            f.Data = append(f.Data, t.Data...)
            return f, nil
        }
    case *SysInfoRootJson:
        if t, ok := tmp.(*SysInfoRootJson); ok {
            f.Data = append(f.Data, t.Data...)
            return f, nil
        }
    case *ForkRootJson:
        if t, ok := tmp.(*ForkRootJson); ok {
            f.Data = append(f.Data, t.Data...)
            return f, nil
        }
    case *KillRootJson:
        if t, ok := tmp.(*KillRootJson); ok {
            f.Data = append(f.Data, t.Data...)
            return f, nil
        }
    case *OpenRootJson:
        if t, ok := tmp.(*OpenRootJson); ok {
            f.Data = append(f.Data, t.Data...)
            return f, nil
        }
    case *RecvRootJson:
        if t, ok := tmp.(*RecvRootJson); ok {
            f.Data = append(f.Data, t.Data...)
            return f, nil
        }
    case *SendRootJson:
        if t, ok := tmp.(*SendRootJson); ok {
            f.Data = append(f.Data, t.Data...)
            return f, nil
        }
    case *SocketRootJson:
        if t, ok := tmp.(*SocketRootJson); ok {
            f.Data = append(f.Data, t.Data...)
            return f, nil
        }
    default:
        return nil, fmt.Errorf("unsupported type: %T", final)
    }

    return nil, fmt.Errorf("types didn't match")
}


func deleteSavedSyscallData(syscall string) error {
    file, err := OpenSyscallDataFile(syscall)
    if err != nil {
        return fmt.Errorf("error opening data file %v for deletion: %v", syscall, err)
    }
    defer file.Close()

    err = file.Truncate(0)
    if err != nil {
        return fmt.Errorf("error clearing data file %v: %v", syscall, err)
    }
 
    return nil
}


func (d *CloseRootJson) SaveToFusionCore(client bridge.OSClient) error {
    for i, data := range d.Data {
        if i >= len(d.TimeStamps) {
            log.Errorf("timestamps were shorter than data")
            return nil
        }

        MSG := bridge.CloseCall{
            TimeStamp: d.TimeStamps[i].Time,
            Pid:       data.Pid,
            Uid:       data.Uid,
            Gid:       data.Gid,
            Count:     data.Count,
            Range:     d.Range,
            DevID:     config.GetID(),
        }

        e := helpers.SaveToIntermediate(&MSG, "os-metadata")
        if e != nil {
            log.Errorf("error logging intermediate data to file: %v", e)
        }

        _, e = client.MarkCloseSysCall(context.Background(), &MSG)
        if e != nil {
            return e
        }
    }

    return nil
}

func (d *SysInfoRootJson) SaveToFusionCore(client bridge.OSClient) error {
    for _, data := range d.Data {

        gRPC_timestamp, err := data.Nsec.ToGRPC()
        if err != nil {
            log.Infof("error creating timestamp from data read in SysInfo file: %v", err)
            continue
        }

        MSG := bridge.SysInfoCall{
            TimeStamp: gRPC_timestamp,
            Pid:       data.Pid,
            Uid:       data.Uid,
            Gid:       data.Gid,
            DevID:     config.GetID(),
        }

        e := helpers.SaveToIntermediate(&MSG, "os-metadata")
        if e != nil {
            log.Errorf("error logging intermediate data to file: %v", e)
        }

        _, e = client.MarkSysInfoSysCall(context.Background(), &MSG)
        if e != nil {
            return e
        }
    }

    return nil
}

func (d *ExecveRootJson) SaveToFusionCore(client bridge.OSClient) error {
    for _, data := range d.Data {

        gRPC_timestamp, err := data.Nsec.ToGRPC()
        if err != nil {
            log.Infof("error creating timestamp from data read in SysInfo file: %v", err)
            continue
        }

        MSG := bridge.ExecveCall{
            TimeStamp: gRPC_timestamp,
            Pid:       data.Pid,
            Uid:       data.Uid,
            Gid:       data.Gid,
            ArgNum:    data.ArgNum,
            Arg:       data.Arg,
            DevID:     config.GetID(),
            At:        d.At,
        }

        e := helpers.SaveToIntermediate(&MSG, "os-metadata")
        if e != nil {
            log.Errorf("error logging intermediate data to file: %v", e)
        }

        _, e = client.MarkExecveSysCall(context.Background(), &MSG)
        if e != nil {
            return e
        }
    }

    return nil
}

func (d *ForkRootJson) SaveToFusionCore(client bridge.OSClient) error {
    for i, data := range d.Data {
        if i >= len(d.TimeStamps) {
            log.Errorf("timestamps were shorter than data")
            return nil
        }

        MSG := bridge.ForkCall{
            TimeStamp: d.TimeStamps[i].Time,
            Pid:    int32(data.Pid),
            Tid:    int32(data.Tid),
            Uid:    int32(data.Uid),
            Gid:    int32(data.Gid),
            Count:  int32(data.Count),
            V:      d.V,
            DevID:  config.GetID(),
        }

        e := helpers.SaveToIntermediate(&MSG, "os-metadata")
        if e != nil {
            log.Errorf("error logging intermediate data to file: %v", e)
        }

        _, e = client.MarkForkSysCall(context.Background(), &MSG)
        if e != nil {
            return e
        }
    }

    return nil
}

func (d *KillRootJson) SaveToFusionCore(client bridge.OSClient) error {
    for i, data := range d.Data {
        if i >= len(d.TimeStamps) {
            log.Errorf("timestamps were shorter than data")
            return nil
        }

        MSG := bridge.KillCall{
            TimeStamp: d.TimeStamps[i].Time,
            Pid:    int32(data.Pid),
            ArgPid: int32(data.ArgPid),
            Uid:    int32(data.Uid),
            Gid:    int32(data.Gid),
            Sig:    int32(data.Sig),
            T:      d.T,
            DevID: config.GetID(),
        }

        e := helpers.SaveToIntermediate(&MSG, "os-metadata")
        if e != nil {
            log.Errorf("error logging intermediate data to file: %v", e)
        }

        _, e = client.MarkKillSysCall(context.Background(), &MSG)
        if e != nil {
            return e
        }
    }

    return nil
}

func (d *OpenRootJson) SaveToFusionCore(client bridge.OSClient) error {
    for i, data := range d.Data {
        if i >= len(d.TimeStamps) {
            log.Errorf("timestamps were shorter than data")
            return nil
        }

        MSG := bridge.OpenCall{
            TimeStamp: d.TimeStamps[i].Time,
            Pid:       int32(data.Pid),
            Uid:       int32(data.Uid),
            Gid:       int32(data.Gid),
            Filename:  data.Filename,
            Count:     int32(data.Count),
            Variation: d.Variation,
            DevID:     config.GetID(),
        }

        e := helpers.SaveToIntermediate(&MSG, "os-metadata")
        if e != nil {
            log.Errorf("error logging intermediate data to file: %v", e)
        }

        _, e = client.MarkOpenSysCall(context.Background(), &MSG)
        if e != nil {
            return e
        }
    }

    return nil
}

func (d *RecvRootJson) SaveToFusionCore(client bridge.OSClient) error {
    for i, data := range d.Data {
        if i >= len(d.TimeStamps) {
            log.Errorf("timestamps were shorter than data")
            return nil
        }

        MSG := bridge.RecvCall{
            TimeStamp: d.TimeStamps[i].Time,
            Pid:            int32(data.Pid),
            Uid:            int32(data.Uid),
            Gid:            int32(data.Gid),
            FileDescriptor: int32(data.Fd),
            Count:          int32(data.Count),
            Variation:      d.Variation,
            DevID: config.GetID(),
        }

        e := helpers.SaveToIntermediate(&MSG, "os-metadata")
        if e != nil {
            log.Errorf("error logging intermediate data to file: %v", e)
        }

        _, e = client.MarkRecvSysCall(context.Background(), &MSG)
        if e != nil {
            return e
        }
    }

    return nil
}

func (d *SendRootJson) SaveToFusionCore(client bridge.OSClient) error {
    for i, data := range d.Data {
        if i >= len(d.TimeStamps) {
            log.Errorf("timestamps were shorter than data")
            return nil
        }

        MSG := bridge.SendCall{
            TimeStamp:      d.TimeStamps[i].Time,
            Pid:            int32(data.Pid),
            Uid:            int32(data.Uid),
            Gid:            int32(data.Gid),
            FileDescriptor: int32(data.Fd),
            Len:            int32(data.Len),
            Count:          int32(data.Count),
            Variation:      d.Variation,
            DevID:          config.GetID(),
        }

        e := helpers.SaveToIntermediate(&MSG, "os-metadata")
        if e != nil {
            log.Errorf("error logging intermediate data to file: %v", e)
        }

        _, e = client.MarkSendSysCall(context.Background(), &MSG)
        if e != nil {
            return e
        }
    }

    return nil
}

func (d *SocketRootJson) SaveToFusionCore(client bridge.OSClient) error {
    for i, data := range d.Data {
        if i >= len(d.TimeStamps) {
            log.Errorf("timestamps were shorter than data")
            return nil
        }

        MSG := bridge.SocketCall{
            TimeStamp: d.TimeStamps[i].Time,
            Pid:       data.Pid,
            Uid:       data.Uid,
            Gid:       data.Gid,
            Family:    data.Family,
            Type:      data.Type,
            Protocol:  data.Protocol,
            Count:     data.Count,
            Pair:      d.Pair,
        }

        e := helpers.SaveToIntermediate(&MSG, "os-metadata")
        if e != nil {
            log.Errorf("error logging intermediate data to file: %v", e)
        }

        _, e = client.MarkSocketSysCall(context.Background(), &MSG)
        if e != nil {
            return e
        }
    }

    return nil
}

