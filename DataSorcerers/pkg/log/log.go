package log;

import (
    "os"
    "io"
    "fmt"
    "github.com/sirupsen/logrus"

    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
)

var log *logrus.Logger;

// CustomHook prepends log messages with additional info
type CustomHook struct {
    prefix string
}

// Fire is called for each log entry
func (h *CustomHook) Fire(entry *logrus.Entry) error {
    entry.Message = h.prefix + entry.Message
    return nil
}

// Levels returns the log levels the hook is applied to
func (h *CustomHook) Levels() []logrus.Level {
    return logrus.AllLevels
}

func AppendLogs(logger *logrus.Logger, msg string) error {
    logger.AddHook(&CustomHook{prefix: msg})
    return nil
}

func AddServiceName(logger *logrus.Logger, name string) error {
    name = "[" + name + "] "
    return AppendLogs(logger, name)
}

func InitializeLogOutput(ServiceName string) error {
    logFile, err := os.OpenFile(config.Settings.LogFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
    if err != nil {
        logrus.Fatalf("failed to open log file: %v", err)
    }

    // Create a MultiWriter that writes to both os.Stdout and the log file
    multiWriter := io.MultiWriter(os.Stdout, logFile)

    // Set the output of the standard logger to the MultiWriter
    logrus.SetOutput(multiWriter)

    // Set up custom logger with service tagging
    logger := logrus.New()
    logger.SetOutput(multiWriter)
    logger.SetFormatter(&logrus.TextFormatter{
        FullTimestamp: true,
        TimestampFormat: "15:04:05",
        ForceColors:   true,
        DisableQuote:  true,
        DisableSorting: false,
    })


    err = AddServiceName(logger, ServiceName)
    if err != nil {
        logrus.Fatalf("failed to add service name to logrus")
    }

    logger.Infof("hello world from %v scraper", ServiceName)

    log = logger; // save it in the global scope

    return nil
}

func Infof(in string, args ...interface{}) {
    log.Info(fmt.Sprintf(in, args...))
}

func Info(args ...interface{}) {
    log.Info(args...)
}

func Errorf(in string, args ...interface{}) {
    log.Error(fmt.Sprintf(in, args...))
}

func Error(args ...interface{}) {
    log.Error(args...)
}

func Fatalf(in string, args ...interface{}) {
    log.Fatal(fmt.Sprintf(in, args...))
}

func Fatal(args ...interface{}) {
    log.Fatal(args...)
}

