package main;

import (
    "fmt"
    "os/exec"
    "strings"
    "strconv"
)


func parseUserDataFromFilename(filePath string) (string, string, int, error)  {
        arr := strings.Split(filePath, ".")
        if len(arr) < 4 { 
            return "", "", -1, fmt.Errorf("Improperly formatted file name: %v", filePath)
        }

        host := arr[1]
        user := arr[len(arr) - 3]
        cnt, err := strconv.Atoi(arr[len(arr) - 2])

        return host, user, cnt, err
}


func ttylogToCsv(filePath string, user string, host string, cnt int) (string, error) {

    if isEmpty, err := fileIsEmpty(filePath); isEmpty || err != nil {
        if err != nil {
            return "", err
        }
        return "", fmt.Errorf("file is empty. simply delete the file")
    }

    csv_filename := fmt.Sprintf("%v-%v-%v.csv", user, host, cnt)

    code := fmt.Sprintf(`/usr/local/src/ttylog/analyze.py %v %v`, 
                            filePath, csv_filename)

    cmd := exec.Command("bash", "-c", code)

    if output, err := cmd.CombinedOutput(); err != nil {
        return "", fmt.Errorf("Error creating csv: %v. Output: %v", err, string(output))
    }

    return csv_filename, nil
}



