package network;

import "sync"
import "strings"
import "context"
import log "github.com/sirupsen/logrus"
import "github.com/influxdata/influxdb-client-go/v2"
import "github.com/influxdata/influxdb-client-go/v2/api"

import bridge "FusionBridge/metadata/network"

import "gitlab.com/instrumentation/FusionCore/pkg/config"


type NetworkServer struct {
    bridge.UnimplementedNetworkServer
    mu sync.Mutex
    writeAPI api.WriteAPIBlocking
    queryAPI api.QueryAPI
}


func NewServer(client influxdb2.Client) *NetworkServer {
    s := &NetworkServer{
        writeAPI: client.WriteAPIBlocking(config.Settings.ORG, config.Settings.BUCKET_NAME),
        queryAPI: client.QueryAPI(config.Settings.ORG),
    }
    return s
}


func (s *NetworkServer) LogNetworkActivity(ctx context.Context, 
    MSG *bridge.NetworkSlice) (*bridge.NetworkACK, error) {

    // I've decided against saving the timestamp sent with the 
        // NetworkSlice itself but am keeping it there in case I 
        // change my mind

    for _, pkt := range MSG.Packets {

        // Write the read data to the writeAPI
        tags := map[string]string{
                "DevID":strings.Trim(strings.TrimSpace(MSG.DevID), "\n"),
                "LinkProtocol": pkt.LinkProtocol,
                "NetworkProtocol": pkt.NetworkProtocol,
                "TransportProtocol": pkt.TransportProtocol,
                "ApplicationProtocol":pkt.ApplicationProtocol,
        }

        fields := map[string]interface{}{
                "Dev":pkt.Dev,
        }

        // Ingest Link layer into into fields
        if pkt.LinkProtocol == "ARP" {
            tags["SrcHwAddy"]  = string(pkt.ARP.SrcHwAddy)
            tags["SrcProtAdd"] = string(pkt.ARP.SrcProtAdd)
            tags["DstHwAddy"]  = string(pkt.ARP.DstHwAddy)
            tags["DstProtAdd"] = string(pkt.ARP.DstProtAdd)
            fields["Operation"]  = pkt.ARP.Operation
            fields["Protocol"]   = pkt.ARP.Protocol
        } else if pkt.LinkProtocol == "Ethernet" {
            tags["SRC_MAC"]  = string(pkt.ETH.SRC_MAC)
            tags["DST_MAC"] = string(pkt.ETH.SRC_MAC)
            fields["Length"]  = string(pkt.ETH.Length)
        }
        // Ingest Network layer into into fields
        if pkt.NetworkProtocol == "IP" {
            tags["SRC_IP"]  = string(pkt.IP.SRC_IP)
            tags["DST_IP"] = string(pkt.IP.SRC_IP)
            if pkt.IP.V4 {
                fields["Version"] = "v4"
            } else {
                fields["Version"] = "v6"
            }
        } else if pkt.NetworkProtocol == "ICMP" {
            // Just here in case
        }
        // Ingest Transport layer into into fields
        if pkt.TransportProtocol == "TCP" {
            fields["SrcPort"] = string(pkt.TCP.SrcPort)
            tags["DstPort"] = string(pkt.TCP.DstPort)
        } else if pkt.TransportProtocol == "UDP" {
            fields["SrcPort"] = string(pkt.UDP.SrcPort)
            tags["DstPort"] = string(pkt.UDP.DstPort)
        }
        // Ingest Application layer into into fields
        if pkt.TransportProtocol == "DNS" {
            fields["Questions"] = pkt.DNS.Questions
            fields["Answers"] = pkt.DNS.Answers
        } else if pkt.TransportProtocol == "TLS" {
            // Here for posterity
        }

        point := influxdb2.NewPoint("network", tags, fields, 
            MSG.TimeStamp.AsTime())
        
        err := s.writeAPI.WritePoint(context.Background(), point)
        
        if err != nil {
            log.Infof("Error in network.LogNetworkActivity: %v", err)
            return &bridge.NetworkACK{
                Type: 1, 
                SubmissionNumber: MSG.SubmissionNumber,
            }, err
        }
    }

    return &bridge.NetworkACK{
        Type: 0, 
        SubmissionNumber: MSG.SubmissionNumber,
    }, nil
}

