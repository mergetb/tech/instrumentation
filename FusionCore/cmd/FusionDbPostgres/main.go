package main

import (
    "time"
    log "github.com/sirupsen/logrus"

    "gitlab.com/instrumentation/FusionCore/pkg/config"
    "gitlab.com/instrumentation/FusionCore/pkg/databases/postgres"
)

func main() {
    err := config.LoadConfig()
    if err != nil {
        log.Fatalf("error loading fusioncore config: %v", err)
    }

    err = postgres.StartDB()
    if err != nil {
        log.Fatalf("error starting postgres database: %v", err)
    }

    err = postgres.Connect()
    if err != nil {
        log.Fatalf("unable to connect to postgres: %v", err)
    }

    log.Info("verifying postgres connection")

    for tmp := postgres.VerifyConnection(); tmp != nil; tmp = postgres.VerifyConnection() {
        log.Info("failed to verify connection to postgres. pausing then retrying")
        time.Sleep(2 * time.Second)
    }
   
    log.Info("started postgres container")
}
