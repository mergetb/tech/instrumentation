package main;


/* 
    We can find ansible config files in 4 places (in the following 
    locations:
        1) $ANSIBLE_CONFIG
        2) ansible.cfg (current directory)
        3) ~/.ansible.cfg
        4) /etc/ansible/ansible.cfg
    for #2 we are going to sweep /home 

    We need to can /home for all .yml files and then check if they
        contain - hosts:
    If the contain this we will save them as "Unverified" ansible
        playbooks. "Verified" playbooks are going to be from bash 
    Verify there aren't any playbooks saved in /etc/ansible
*/


import (
    "fmt"
    "time"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


func main() {
    config.LoadConfig()
    if !config.Settings.RunAnsible { return }

    err := log.InitializeLogOutput("Ansible")
    if err != nil {
        fmt.Printf("error initializing logging: %v", err)
        return
    }

    err = ConnectToFusionCore()
    if err != nil {
        log.Fatal("failed to connect to fusion core: %v", err)
    }

    if config.Settings.LogToIntermediateFile {
        err = helpers.BuildDir(config.Settings.IntermediateDataFolder)
        if err != nil {
            log.Fatalf("error building intermediate data dir: %v", err)
        }
    }

    // Determine which should run first, how long to pause between runs, etc
    // Basically are we ScrapeConfigs or ScrapePlaybooks and how long between each
    // Probably should have just threaded this, but here we are. I like algos
    first_interval, first_func, second_interval, second_func := GetAnsibleSweepOrder()
   
    // Loop to actually record things
    for {
        first_func()
        time.Sleep(time.Second * time.Duration(first_interval))
        second_func()
        time.Sleep(time.Second * time.Duration(second_interval))

        log.Info("finished reading. looping")
    }
}


func GetAnsibleSweepOrder() (int, func(), int, func()) {
    // Man I'm sorry for this. I hate nested logic but this is minimal
    // I miss the ternary operator. This could be 4 wonderous lines

    var first_interval, second_interval int;
    var first_func, second_func func()

    if config.Settings.SaveAnsiblePlaybooks && config.Settings.SaveAnsibleConfigs {
        if config.Settings.AnsibleConfigSweepInterval > config.Settings.AnsiblePlaybookSweepInterval {
            first_interval  = config.Settings.AnsiblePlaybookSweepInterval
            second_interval = config.Settings.AnsiblePlaybookSweepInterval - config.Settings.AnsibleConfigSweepInterval

            first_func = ScrapeConfigs
            second_func = ScrapePlaybooks
        } else {
            first_interval  = config.Settings.AnsibleConfigSweepInterval
            second_interval = config.Settings.AnsibleConfigSweepInterval - config.Settings.AnsiblePlaybookSweepInterval 

            first_func = ScrapePlaybooks
            second_func = ScrapeConfigs
        }
    } else if config.Settings.SaveAnsiblePlaybooks {
        first_interval  = config.Settings.AnsiblePlaybookSweepInterval
        first_func = ScrapePlaybooks
    } else if config.Settings.SaveAnsibleConfigs {
        first_interval  = config.Settings.AnsibleConfigSweepInterval
        first_func = ScrapeConfigs
    } else {
        log.Fatal("failed to determine which scrapping should occur first. exiting")
    }

    return first_interval, first_func, second_interval, second_func
}

