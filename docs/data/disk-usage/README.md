# Data Sizes for Each Measurement

There are 4 possible data types for an influx measurement: `Float`, `Integer`, `String`, and `Boolean`

All tags are of the type `String`

Influxdb2 uses UTF-8 for its `String` data type. This should mean 1 byte per character, but there 
could be special characters.


# Empirical Trends in Data Size and Disk Usage Growth

This is going to be a guide on how the database grows under different conditions, to give some 
intuitive understanding of how large the data is and how fast it can grow.

The programs in analysis-scripts are meant to be run in `docs/data/disk-usage`, there is relative 
pathing, sorry.


## Baseline

These are going to be run on the docker container and the merge testbed, to set baselines for how 
quickly the databases grow, without any explicit user interaction.



### Docker

A number of the sorcerers are going to be left out of this environment, since they have trouble 
running in the docker environment and aren't the priority to get working.

#### Network Sorcerer

Databases grew by 0.96KBps

#### File Sorcerer

Databases grew by 0.0KBps

#### Network and File Sorcerers

Databases grew by 1.2KBps



### MergeTB

This a star topology in the sphere test bed where only 1 node is sending data to the FusionCore.

#### Ansible Sorcerer




#### Bash Sorcerer




#### Jupyter Sorcerer



#### Logs

Databases grew by  0.44KBps

0.36KBps was due to influx and 0.08Kpbs was stored in postgres

**Note:** The postgres logs actually grew much faster than the influx logs, but stop at a max 
value rather quickly, only increasing by ~32KB over the entire trial. After the initialization 
period, the logs stop expanding and the postgres schema prevented the data from blowing up very 
large. Quite handy.

**Note:** We can very clearly see the increase in the DB size due to the logs, since the log 
dumping is on a timer. We can reduce the KBps increase in the influx db by simply increasing the 
time between dumps.


#### ID Metadata Sorcerer



#### Network Sorcerer

Databases grew by 0.48KBps


#### File Metadata Sorcerer

Databases grew by 0.24KBps


#### OS

Databases grew by


#### Network and File Sorcerer

Databases grew by 1.8KBps



## Simple Pings

If you are simply pinging a discern node from another machine, there are three types of data 
that could be recorded: network traffic and some system calls. 

We are going to turn on the sorcerers individually to see how the operations grow the database, 
giving us a more intuitive understanding for the size of the data. 

While I could just verify the size of the datatypes used, the situations aren't nearly as clean as 
you'd image. Take this ping example. We could just count the size of the objects storing the 
ping's data and call it a day, but this wouldn't include the occasional arp packet sent, meaning 
our data is growing faster than we initially anticipated. While this example is small and doesn't 
REALLY cause that big of a difference, it does prove that we need more emperical data.


To take this data, pings were sent from the local machine to a single "discernpod", whether that 
be a docker container or a proper materialization node. The influx containers disk usage was 
queried every 0.5s, to see how quickly the database and related files were growing. There was a 
"startup" and "cooldown" period, to verify the database was only growing due to the specific 
sorcerers.

Now that I've lightly justified my approach, lets look at the data!


### Docker

This data was collected in the testing docker environment. In theory, this will give us the 
closest to ground truth for how large the ping data actually is. Any testing done in the testbed 
should also have infra-net communications and more background processes, so it will be noisier. 
And while that's important data, its fundamentally different data.


#### Network Only Ping Data

To see the data run `analysis-scripts/simple-pings.py`. From this data, we can see that the data 
grows fairly linearly, which we'd expect, and grows by about 7.8KB per second.

**Note:** The axis is DOUBLE the test time, since data is taken every half second.

**Result:** Ping data grows by about 7.8KB per second on low traffic rates. This is almost 
definitely an issue


#### File Only Ping Data

This time only the File Data Sorcerer was running while the pings were sent.

To see the data run `analysis-scripts/simple-pings.py`. From this data, we can see that the data 
doesn't increase at all. There are no recorded file system effects on recieving a ping packet.

**Result:** File data grows at 0.0KB per second with low traffic rates


#### Network and File Ping Data

Both the Network and File Data Sorcerers were running while the pings were sent.

To see the data run `analysis-scripts/simple-pings.py`. From this , we can see that the data grows 
fairly linearly, which we'd expect, and grows by about 8.45KB per second.

**Note:** This is larger than the network packets. Why? Because the File Data is still being sent
to the FusionCore and gets recorded as network data. The File only data is effectly a 0.0KB 
addition to the database, but that doesn't mean its exactly zero. The file Sorcerer still contacts
the Fusion Core, which increases the rate the database grows at.

**Result:** Adding more data sorcerers increases the rate the database grows at.


### Merge TB

This data was collected on a basic star topology in the sphere testbed. A single node is recording
and logging data to the FusionCore. This will give us our most accurate data collection rates, but 
should give us less accurate information about the size of the data, since it will contain a lot 
more noise.


#### Network Only Data

The databases grow at 4.64KB per second.

**Note:** While the initial assumption about the docker container being less noisy seems valid, 
thats actually not the case. The docker networking appears to be fundamentally noisier. 
Interesting

**Note:** We can definitely reduce how fast the network data increases the DB size by preventing 
it from recording data to the fusion core. That could be tricky though

#### File Only Data

The databases grow at 0.2KB per second. This makes sense as the OS is more fleshed out, and it 
should be performing more file system operations. 

**Note:** This should be investigated further!

#### Network and File Data

The databases grow at 5.88KB per second. This is caused by an increase in the number of file 
operations happening and the inclusion of a second set of dat on top of the networking data.

