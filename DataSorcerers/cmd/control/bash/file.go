package main;

import (
    "os"
    "fmt"
    "bufio"
    "strings"
)


func getTTYLogFilenames() ([]string, error) {
    files, err := os.ReadDir("/var/log/ttylog")
    if err != nil {
        return nil, err
    }

    ret := []string{}

    for _, file := range files {
        ret = append(ret, file.Name())
    }

    return ret, nil
}


func fileIsEmpty(filePath string) (bool, error) {
    // Open the file
    file, err := os.Open(filePath)
    if err != nil {
        return false, fmt.Errorf("error opening file: %v", err)
    }
    defer file.Close()

    // Get file info
    fileInfo, err := file.Stat()
    if err != nil {
        return false, fmt.Errorf("error getting file info: %v", err)
    }

    // Check if the file is empty
    if fileInfo.Size() == 0 {
        return true, nil
    } 

    return false, nil
}


func emptyTTYLog(filePath string) error {

    fd, err := os.OpenFile(filePath, os.O_RDWR, 0666)
    if err != nil {
        return err
    }
    defer fd.Close()


    // Check if the last line of a file starts with "END"
    if over, err := isSessionOver(fd); over || err != nil {
        if over { // If its over and has been sent up, remove the file
            fd.Close()
            return os.Remove(filePath)
        } else {
            return fmt.Errorf("error determining if %v session is over: %v", filePath, err)
        }
    }

    // Get first 3 lines of the file. Those are required for parsing
    prelude, err := getFilePrelude(fd)
    if err != nil {
        return fmt.Errorf("error reading %v prelude: %v", filePath, err)
    }

    // Remove everything from the file
    fd.Truncate(0)
    fd.Seek(0, 0)

    // Write the prelude back in
    writer := bufio.NewWriter(fd)
    for _, line := range prelude {
        _, err := writer.WriteString(line + "\n")
        if err != nil {
            return fmt.Errorf("error writing prelude to %v: %v", filePath, err)
        }
    }

    err = writer.Flush()
    if err != nil {
        return fmt.Errorf("error writing prelude back to %v: %v", filePath, err)
    }

    return nil
}


func isSessionOver(fd *os.File) (bool, error) {
    var lastLine string

    // Read the file line by line
    scanner := bufio.NewScanner(fd)
    for scanner.Scan() {
        lastLine = scanner.Text() // Update lastLine with each new line
    }

    if err := scanner.Err(); err != nil {
        return false, err
    }

    if strings.TrimSpace(lastLine)[:3] == "END" {
        return true, nil
    }

    return false, nil
}


func getFilePrelude(fd *os.File) ([]string, error) {
    fd.Seek(0, 0)

    prelude := []string{}
    scanner := bufio.NewScanner(fd)
    for i := 0; i < 3 && scanner.Scan(); i++ {
        prelude = append(prelude, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        return nil, err
    }

    return prelude, nil
}

