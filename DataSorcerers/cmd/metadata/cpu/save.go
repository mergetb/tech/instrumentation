package main;

import (
    "fmt"
    "time"
    "context"
    "google.golang.org/grpc" 
    "github.com/golang/protobuf/ptypes"

    bridge "FusionBridge/metadata/cpu"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)

var opts []grpc.DialOption;
var conn *grpc.ClientConn;
var err error; 
var client bridge.CpuClient;


func ConnectToFusionCore() error {
    conn = helpers.CreateConnection()
    client = bridge.NewCpuClient(conn); 

    return nil
}


func SaveCPULoad(percents []float64) error {
    timestamp, _ := ptypes.TimestampProto(time.Now())

    ctx, cancel := context.WithTimeout(context.Background(), 
        10*time.Second)
    defer cancel()


    MSG := bridge.CpuLoad{
        SubmissionNumber:0,
        TimeStamp:timestamp,
        Load: percents,
        DevID: config.GetID(),
    }


    e := helpers.SaveToIntermediate(&MSG, "cpu-load")
    if e != nil {
        log.Errorf("error logging intermediate data to file: %v", e)
    }

    _, e = client.SaveCpuLoad(ctx, &MSG)
    if e != nil {
        return fmt.Errorf("client error saving cpu event: %v", e)
    }

    return nil
}

