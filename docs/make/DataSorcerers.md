# Building the Data Sorcerers

**This documentation assumes you are in the `DataSorcerers` directory unless otherwise specified**

**This guide also assumes you have install all the dependencies with `./dep.sh build`**

The Data Sorcerers are simply a series of stand alone binaries and bash scripts. Some sorcerers
are complex and need to be split into 2 parts, a recording part and an exfiltration (called dumping) 
part. The only sorcerers split into recording and dumping are the bash sorcerer and the os 
sorcerer.

The actual programs you need to run are 

	build/discern-validation-sorcerer
	build/discern-ansible-sorcerer
	build/discern-bash-sorcerer
	build/discern-jupyter-sorcerer
	build/discern-logs-sorcerer
	build/discern-file-sorcerer
	build/discern-network-sorcerer
	build/discern-os-sorcerer
    DataSorcerers/dist/bash-logs/start_ttylog.sh

If you simply want to make the binaries, install them on the system, and start them, run:

```
make all
make install
make enable
```

<br />

## Building the Binaries

To build all these programs, you can run

```
make all
```

<br />

## Installing the Sorcerers

The Data Sorcerers expect certain directories to exist in the file system, to save data and things
of that nature. To set up the directories in the file system, you can run:

```
make build-directories
```

The Sorcereres are just binaries wrapped with systemd. Its not necessary to use systemd, so this 
is an option part of the install process. 

To install the binaries run:

```
make install-binaries
```

To install the systemd services, run:

```
make install-services
```

To do all of this in a single command, run:

```
make install
```

<br />

## Enabling the Data Sorcerers

To start the sorcerers you need to enable 2 parts, the bpftrace programs (for the os sorcerer) and 
the systemd services.

To enable and start the systemd services, run:

```
make enable-services
```

To start the bpftrace programs run:

```
make enable-os-trace
```

To do this in one step, run:

```
make enable
```

<br />

## Building the Debian

We also have a debian package, which can be used to easily install all the Data Sorcerers. To 
build that, run:

```
make build/deb
```

The debian can then be installed with:

```
sudo dpkg -i ./DataSorcerers.deb
```

<br />

## Restarting the Data Sorcerers

To easily restart the Data Sorcerers, we can use:

```
make restart
```

This is only really meant for testing and is a simple wrapper around `systemctl`

<br />

## Uninstalling the Data Sorcerers

To uninstall and disable all the programs, we have:

```
make uninstall
```

