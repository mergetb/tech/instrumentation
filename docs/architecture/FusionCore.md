# Fusion Core Architecture

This architecture of this server is intentionally quite basic:

- A large server to create listeners for and store all the data 
    from the gRPC services
    - The specifications of these services can be found at ./sorcerers. 
        Each service is activated independently on a client node and 
        none are required to be run

- An influxDB instance running in a docker container to store all the 
    time series data from the gRPC services

- A postgres instance running in a docker container to store any large files

This architecture is controlled entirely with a config file located at
    /etc/discern/CoreConfig.yaml

The server itself logs all information to /var/log/discern.log

The databases logs information inside their docker containers using the
    default settings

This program also features a systemd service which one can easily install
    using `cd FusionCore; make build/FusionCore; make install`

    - If you don't need the system service, then `cd FusionCore; make build/FusionCore;` works 
    just as wellb



## Database Architecture

The details on all the data downloaded, where its stored, and how its stored can be found in 
`docs/data/format`

The FusionCore looks for the databases at the IPs and Ports specified in `CoreConfig.yaml` and 
authenticates with the information provided there. 

The FusionCore assumes that the postgres instance has the schemas defined in 
`FusionCore/dist/docker/postgres/schema`. But the influx instance doesn't force an org or bucket 
name, those are controlled in the config file.

The Fusion Core will save to a docker volume named `discernpsql`. If you need to manipulate the
data in any way (delete the data, copy it, etc), this is up to you and should be done via the 
docker cli

The Fusion Core will also create a docker volume named `discerninflux` to store persistent influx
data, if you'd like to copy it, delete it, exfiltrate it, etc. Those operations should also be 
done via the docker cli



## Saving Data from Data Sorcerers

Each data sorcerers communicates with a unique GRPC server, which gets set up in the FusionCore's
main loop. 

Each server is defined in FusionCore/pkg/ingest, using the data types defined in the FusionBridge,
and its only job is to sanitize the data recieved from a single type of Data Sorcerers and save 
that to the appropriate database.

