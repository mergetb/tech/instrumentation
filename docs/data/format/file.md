# Saved File Metadata

Whenever a new file system event occurs (chown, chmod, touch, etc), a record is logged into the 
influx database as a "file" measurement.

This record saves information about the file's:

- DevID (a string uniquely IDing every machine in the testbed. ie what device is the file on)
- Owner
- Location
- Op (ie what file system event actually occured)
- Contents (We no longer collect this, but the code to add this feature back in still exists)
- Permissions
- Group


`DevID` & `Owner` & `Location` & `Op` are all tags

`Content`, `Permissions`, & `Group` are all fields

