package config;

import (
    "strings"
    "strconv"
    "io/ioutil"
    "gopkg.in/yaml.v2"
    "github.com/sirupsen/logrus"
)

var Settings YamlConfig = *NewYamlConfig();
var ID_STRING string = "";


type YamlConfig struct {
    // Logging information
    LogFile                        string `default:"/var/log/discern.log"`
    // Intermiediate Data location & stuff
    IntermediateDataFolder         string `default:"/var/log/discern/data"`
    LogToIntermediateFile          bool   `default:true`
    // For gRPC connection
    GRPC_Port                      int64  `default:50051`
    GRPC_IP                        string `default:"localhost"`
    GRPC_PROTO                     string `default:"tcp"`
    MaxRecvMsgSize                 int  `default:10485760`
    // For TLS connection
    TLS                            bool   `default:false`
    CertFile                       string `default:""`
    KeyFile                        string `default:""`
    // For ID section
    IdApiUrl                       string `default:"localhost:50051/api/id"`
    IdApiBody                      string `default:""`
    RunIdApiServer                 bool `default:true`
    // Specifying which service should run
    RunLog                         bool `default:true`
    RunAnsible                     bool `default:true`
    RunBash                        bool `default:true`
    RunJupyter                     bool `default:true`
    RunFile                        bool `default:true`
    RunId                          bool `default:true`
    RunIdServer                    bool `default:true`
    RunNetwork                     bool `default:true`
    RunOs                          bool `default:true`
    // For metadata/os
    OsInfoDumpInterval             int `default:3001`
    OsDataDir                      string `default:"/tmp/discern/data/os"`
    // For metadata/file
    BlackListDirs                  []string `default:[]string{"/home/.*/\.config","/home/.*/\..*"}`
    StartingDirs                   []string `default:[]string{"/home","/tmp"}`
    // For metadata/network
    NetworkSliceLength             uint `default:5`
    // For Log info
    LogSweepInterval               int `default:5`
    LogLinesCaptured               int `default:5`
    LogFiles                       []string `default:[]string{"/var/log/messages", "/var/log/syslog", "/var/log/auth.log", "/var/log/daemon.log", "/var/log/kern.log"}`
    // For jupyter data
    JupyterSweepInterval           int `default:5`
    JupyterPaths                   []string `default:[]string{"/home"}`
    // For ansible data
    SaveAnsibleConfigs             bool `default:false`
    SaveAnsiblePlaybooks           bool `default:false`
    AnsibleConfigSweepInterval     int `default:5`
    AnsiblePlaybookSweepInterval   int `default:5`
    // For bash logging data
    BashSweepInterval              int `default:5`
    // For ID information
    ScrapeForUsers                 int `default:30000`
    ScrapeInterfaceInfo            int `default:3000`
    // For CPU information
    RunCpu                         bool `default:true`
    CpuInterval                    int `default:5`
}


func NewYamlConfig() *YamlConfig {
    return &YamlConfig{
        LogFile:                      "/var/log/discern.log",

        IntermediateDataFolder:       "/var/log/discern/data",
        LogToIntermediateFile:        true,

        GRPC_Port:                    50051,
        GRPC_IP:                      "localhost",
        GRPC_PROTO:                   "tcp",
        MaxRecvMsgSize:               10485760,

        TLS:                          false,
        CertFile:                     "",
        KeyFile:                      "",

        IdApiUrl:                     "http://localhost:50052/api/id",
        IdApiBody:                    "",
        RunIdApiServer:               true,

        RunLog:                       true,
        RunAnsible:                   true,
        RunBash:                      true,
        RunJupyter:                   true,
        RunFile:                      true,
        RunId:                        true,
        RunIdServer:                  true,
        RunNetwork:                   true,
        RunOs:                        true,

        OsInfoDumpInterval:           3001,
        OsDataDir:                    "/tmp/discern/data/os",

        BlackListDirs:                []string{`/home/.*/\.config`, `/home/.*/\..*`},
        StartingDirs:                 []string{"/home", "/tmp"},

        NetworkSliceLength:           5,

        LogSweepInterval:             5,
        LogLinesCaptured:             5,
        LogFiles:                     []string{"/var/log/messages", "/var/log/syslog", "/var/log/auth.log", "/var/log/daemon.log", "/var/log/kern.log"},

        JupyterSweepInterval:         5,
        JupyterPaths:                 []string{"/home"},

        SaveAnsibleConfigs:           true,
        SaveAnsiblePlaybooks:         true,
        AnsibleConfigSweepInterval:   5,
        AnsiblePlaybookSweepInterval: 5,

        BashSweepInterval:            5,

        ScrapeForUsers:               30000,
        ScrapeInterfaceInfo:          3000,

        RunCpu:                       true,
        CpuInterval:                  5,
    }
}


func (in *YamlConfig) ServerAddr() string {
    return in.GRPC_IP + ":" + strconv.FormatInt(in.GRPC_Port, 10)
}


func LoadConfig() {
    yamlFile, err := ioutil.ReadFile("/etc/discern/SorcererConfig.yaml")
    if err != nil {
        logrus.Fatalf("error loading sorcerer config file %v", err)
    }

    err = yaml.Unmarshal(yamlFile, &Settings)
    if err != nil {
        logrus.Fatalf("error unmarshaling sorcerer config file: %v")
    }

    hostname, err := ioutil.ReadFile("/proc/sys/kernel/hostname")
    if err != nil {
        logrus.Fatalf("error loading hostname from /proc/sys/kernel/hostname: %v", err)
    }

    ID_STRING = strings.ReplaceAll(string(hostname), "\n", "")
}


func GetID() string {
    return ID_STRING;
}

