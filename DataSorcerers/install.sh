#!/bin/bash

build-directories() {
    mkdir -p /etc/discern
    mkdir -p /etc/discern/bpfsensors
    mkdir -p /var/log/ttylog
    mkdir -p /tmp/discern
    mkdir -p /tmp/discern/data
    mkdir -p /tmp/discern/data/os
}

setup-filesystem() {
    if [ ! -f "/etc/discern/SorcererConfig.yaml" ]; then
        cp dist/conf/SorcererConfig.yaml /etc/discern
    fi
    cp dist/bpftrace/tracers/* /etc/discern/bpfsensors/
}

install-binaries() {
    cp ./build/*-sorcerer /usr/local/bin
    ./dist/bash-logs/install.sh
    cp ./dist/bash-logs/start_ttylog.sh /usr/local/src/start_ttylog.sh
    cp ./dist/bpftrace/run_sensors.sh /usr/local/bin/discern-bpftrace-sorcerer
}

install-services() {
    cp ./dist/systemd/* /etc/systemd/system
    systemctl daemon-reload
}

start-services() {
    systemctl start discern-ansible.service && systemctl start discern-ansible.service
    systemctl start discern-bash-dump.service && systemctl start discern-bash-dump.service
    systemctl start discern-bash-record.service && systemctl start discern-bash-record.service
    systemctl start discern-file.service && systemctl start discern-file.service
    systemctl start discern-jupyter.service && systemctl start discern-jupyter.service
    systemctl start discern-logs.service && systemctl start discern-logs.service
    systemctl start discern-network.service && systemctl start discern-network.service
    systemctl start discern-os-dump.service && systemctl start discern-os-dump.service
    systemctl start discern-os-record.service && systemctl start discern-os-record.service
    systemctl start discern-cpu.service && systemctl start discern-cpu.service
    systemctl daemon-reload
}

enable-services() {
    systemctl enable discern-ansible.service && systemctl start discern-ansible.service
    systemctl enable discern-bash-dump.service && systemctl start discern-bash-dump.service
    systemctl enable discern-bash-record.service && systemctl start discern-bash-record.service
    systemctl enable discern-file.service && systemctl start discern-file.service
    systemctl enable discern-jupyter.service && systemctl start discern-jupyter.service
    systemctl enable discern-logs.service && systemctl start discern-logs.service
    systemctl enable discern-network.service && systemctl start discern-network.service
    systemctl enable discern-os-dump.service && systemctl start discern-os-dump.service
    systemctl enable discern-os-record.service && systemctl start discern-os-record.service
    systemctl enable discern-cpu.service && systemctl start discern-cpu.service
    systemctl daemon-reload
}

while [[ $# -gt 0 ]]; do
    case $1 in
        install-binaries)
            install-binaries
            shift
            ;;
        install-services)
            install-services
            shift
            ;;
        build-directories)
            build-directories
            shift
            ;;
        setup-filesystem)
            setup-filesystem
            shift
            ;;
        start-services)
            start-services
            shift
            ;;
        enable-services)
            enable-services
            shift
            ;;
        *) 
            echo "Unknown cli argument to DataSorceres/install $1. Exiting"
            exit 1
    esac
done

