# Building the Fusion Bridge


`make all` is a shorthand to build everything

Explicitly, it builds the grpc communication protocols for a variety of clients that communicate
with the FusionCore's server.

Each one of these is a filename that defines a communication protocol. These are the files that actually get built:

- validation/validation.pb.go
- control/ansible/ansible.pb.go
- control/bash/bash.pb.go
- control/jupyter/jupyter.pb.go
- log/log.pb.go
- metadata/id/id.pb.go
- metadata/file/file.pb.go
- metadata/network/network.pb.go
- metadata/os/os.pb.go

The bridge doesn't run a server or anything like that. It simply standardize the communication 
between the client and the server and allows us to automatically generate the server code in the 
FusionCore and the client protocols in the DataSorcerers.

