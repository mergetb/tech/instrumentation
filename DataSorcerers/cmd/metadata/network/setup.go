package main;

import (
    "fmt"
    "slices"
    "github.com/google/gopacket/pcap"
)


func inactiveSetUp(dev string) (*pcap.InactiveHandle, error) {
    inact_handle, err := pcap.NewInactiveHandle(dev);
    if err != nil {
        return nil, fmt.Errorf("error setting up new inactive handle: %v", err)
    }

    // Can strip errors with: inact_handle.Error()
    inact_handle.SetBufferSize(150000); // in bytes

    // packets are delivered to application directly ASAP
    // overrides SetTimeout
    inact_handle.SetImmediateMode(true); 

    // inact_handle.SetPromisc(true);

    // Same idea as SetPromisc but for wireless networks
    // inact_handle.SetRFMon(true);

    // Set read timeout for the handle
    // inact_handle.SetTimeout(1000)

    // Tell pcap how to set timestamps. Idk what these are though
    // ts, err := pcap.TimestampSourceFromString("i have no idea")
    // inact_handle.SetTimestampSource(ts)

    // List supported timestamp sources:
    ts_src_slice := inact_handle.SupportedTimestamps()
    

    adapt_src, err := pcap.TimestampSourceFromString("adapter")
    if err != nil { 
        return nil, fmt.Errorf("error timestamp source from string: %v", err)
    }

    host_src, err := pcap.TimestampSourceFromString("host")
    if err != nil { 
        return nil, fmt.Errorf("error timestamp source from string: %v", err)
    }

    if slices.Contains(ts_src_slice, adapt_src) {
        inact_handle.SetTimestampSource(adapt_src)
    } else if slices.Contains(ts_src_slice, host_src) {
        inact_handle.SetTimestampSource(host_src)
    }

    return inact_handle, nil
}
