package main;

import (
    "fmt"
    "net"
    "sync"
    "time"
    "context"
    "strings"
    "net/url"
    "google.golang.org/grpc" 
    "github.com/google/gopacket"
    "github.com/google/gopacket/pcap"
    "github.com/golang/protobuf/ptypes"
    "github.com/google/gopacket/layers"

    bridge "FusionBridge/metadata/network"
    fbpacket "FusionBridge/metadata/packet"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


type DataManager struct {
    opts []grpc.DialOption
    conn *grpc.ClientConn
    client bridge.NetworkClient
    NetworkSlice []fbpacket.Packet
    Index uint
    MaxCaptureLength uint
    Devices []pcap.Interface
    SliceMutex sync.Mutex
    FusionCoreIP string
}

func (n *DataManager) ConnectToFusionCore() error {
    n.conn = helpers.CreateConnection()
    n.client = bridge.NewNetworkClient(n.conn); 

    return nil
}

func (n *DataManager) LoadSettings() error {
    n.MaxCaptureLength = config.Settings.NetworkSliceLength

    n.NetworkSlice = make([]fbpacket.Packet, n.MaxCaptureLength)

    n.Index = 0

    fusionIP, err := toIP(config.Settings.GRPC_IP)
    if err != nil {
        return fmt.Errorf("error converting Fusion Core's URL/IP to an IP string: %v", err)
    }

    n.FusionCoreIP = fusionIP

    return nil
}

func toIP(input string) (string, error) {
	// Check if the input is a valid IP
	if net.ParseIP(input) != nil {
		return input, nil
	}

	// Try to parse as URL
	parsedURL, err := url.Parse(input)
	if err != nil || parsedURL.Scheme == "" || parsedURL.Host == "" {
		// Assume the input is a plain hostname if URL parsing fails
		parsedURL = &url.URL{Host: input}
	}

	// Extract the host
	host := parsedURL.Host

	// Strip port if present (e.g., "example.com:80")
	if strings.Contains(host, ":") {
		host, _, err = net.SplitHostPort(host)
		if err != nil {
			return "", fmt.Errorf("failed to parse host: %v", err)
		}
	}

	// Resolve the hostname to IPs
	ips, err := net.LookupIP(host)
	if err != nil {
		return "", fmt.Errorf("failed to resolve host to IP: %v", err)
	}

	// Return the first resolved IP
	if len(ips) > 0 {
		return ips[0].String(), nil
	}

	return "", fmt.Errorf("no IPs resolved for host: %s", host)
}

func (n *DataManager) FindAllDevices() error {
    interfac, err := pcap.FindAllDevs()
    if err != nil {
        return fmt.Errorf("failed to find network devices: %v", err)
    }

    n.Devices = interfac

    return nil
}

func (n *DataManager) Listen() error {
    for _, el := range n.Devices {
        // Create SetUp we can listen off of
        inact_handle, err := inactiveSetUp(el.Name)
        if err != nil {
            log.Errorf("Error creating inactive handle on dev %v: %v", el.Name, err)
            continue
        }
        defer inact_handle.CleanUp()

        // Activate that Set up
        handle, err := inact_handle.Activate()
        if err != nil {
            log.Errorf("error activating handle on dev %v: %v", el.Name, err)
            continue
        }
        defer handle.Close()

        // Log the packets
        source := gopacket.NewPacketSource(handle, handle.LinkType())
        go n.ReadPackets(el.Name, source)
    }

    return nil
}

func (n *DataManager) ReadPackets(name string, source *gopacket.PacketSource) {
    for pkt := range source.Packets() {
        if err := pkt.ErrorLayer(); err != nil {
            log.Errorf("error reading packet on %v: %v", name, err)
            continue
        }

        // Ignore packets sent to Fusion Core
        if ipv4 := pkt.Layer(layers.LayerTypeIPv4); ipv4 != nil {
            ip := ipv4.(*layers.IPv4)
            if ip.SrcIP.String() == n.FusionCoreIP || ip.DstIP.String() == n.FusionCoreIP {
                continue
            }
        }

        timestamp := pkt.Metadata().CaptureInfo.Timestamp

        // Build datatypes by hand. What fun. Long, boring, effective
        // If you have a better way, please make an issue and let me know
        packet_to_send := fbpacket.Packet{
            ApplicationLayer: fbpacket.ApplicationLayer{
                TransportLayer: fbpacket.TransportLayer{
                    NetworkLayer: fbpacket.NetworkLayer{
                        LinkLayer: fbpacket.LinkLayer{
                            PhysicalLayer: fbpacket.PhysicalLayer{
                                TimeStamp : timestamp,
                                Dev       : name,
                            },
                        },
                    },
                },
            },
        };

        if arpPkt := pkt.Layer(layers.LayerTypeARP); arpPkt != nil {
            packet_to_send.LinkLayer.LinkProtocol = "ARP"

            arp := arpPkt.(*layers.ARP)

            packet_to_send.LinkLayer.ARP = fbpacket.ARPPacket{
                 SrcHwAddy  : arp.SourceHwAddress,
                 SrcProtAdd : arp.SourceProtAddress,
                 DstHwAddy  : arp.DstHwAddress,
                 DstProtAdd : arp.DstProtAddress,
                 Operation  : arp.Operation,
                 Protocol   : arp.Protocol,
            }
        } else if ethPkt := pkt.Layer(layers.LayerTypeEthernet); ethPkt != nil {
            packet_to_send.LinkLayer.LinkProtocol = "Ethernet"

            eth := ethPkt.(*layers.Ethernet)

            packet_to_send.LinkLayer.ETH = fbpacket.ETHPacket{
                SRC_MAC : eth.SrcMAC.String(),
                DST_MAC : eth.DstMAC.String(),
                Length  : eth.Length,
            }
        }

        if ipv4 := pkt.Layer(layers.LayerTypeIPv4); ipv4 != nil {
            packet_to_send.NetworkLayer.NetworkProtocol = "IPv4"

            ip := ipv4.(*layers.IPv4)

            packet_to_send.NetworkLayer.IP = fbpacket.IPPacket{
                V4     : true,
                SRC_IP : ip.SrcIP.String(),
                DST_IP : ip.DstIP.String(),
            }
        } else if ipv6 := pkt.Layer(layers.LayerTypeIPv6); ipv6 != nil {
            packet_to_send.NetworkLayer.NetworkProtocol = "IPv6"

            ip := ipv6.(*layers.IPv6)

            packet_to_send.NetworkLayer.IP = fbpacket.IPPacket{
                V4     : false,
                SRC_IP : ip.SrcIP.String(),
                DST_IP : ip.DstIP.String(),
            }
        }

        if tcp := pkt.Layer(layers.LayerTypeTCP); tcp != nil {
            packet_to_send.TransportLayer.TransportProtocol = "TCP"

            pkt := tcp.(*layers.TCP)

            packet_to_send.TransportLayer.TCP = fbpacket.TCPPacket{
                SrcPort : pkt.SrcPort,
                DstPort : pkt.DstPort,
            }

        } else if udp := pkt.Layer(layers.LayerTypeUDP); udp != nil {
            packet_to_send.TransportLayer.TransportProtocol = "UDP"

            pkt := udp.(*layers.UDP)

            packet_to_send.TransportLayer.UDP = fbpacket.UDPPacket{
                SrcPort : pkt.SrcPort,
                DstPort : pkt.DstPort,
            }
        }

        if icmpPkt := pkt.Layer(layers.LayerTypeICMPv4); icmpPkt != nil {
            packet_to_send.TransportLayer.TransportProtocol = "ICMP"
            packet_to_send.TransportLayer.ICMP = fbpacket.ICMPPacket{}

        } else if icmp := pkt.Layer(layers.LayerTypeICMPv6); icmp != nil {
            packet_to_send.TransportLayer.TransportProtocol = "ICMP"
            packet_to_send.TransportLayer.ICMP = fbpacket.ICMPPacket{}
        }

        if tls := pkt.Layer(layers.LayerTypeTLS); tls != nil {
            packet_to_send.ApplicationProtocol = "TLS"
            packet_to_send.TLS = fbpacket.TLSPacket{}
        }


        if dnsPkt := pkt.Layer(layers.LayerTypeDNS); dnsPkt != nil {
            packet_to_send.ApplicationProtocol = "DNS"

            dns := dnsPkt.(*layers.DNS)

            // Minimal info from the DNS queries
            packet_to_send.DNS = fbpacket.DNSPacket{
                Questions : dns.Questions,
                Answers   : dns.Answers,
            }
        }

        err := n.AddToNetworkSlice(packet_to_send)
        if err != nil {
            log.Errorf("error adding to network slice: %v", err)
        }
    }
}

func (n *DataManager) SaveNetworkSlice() error {
    timestamp, err := ptypes.TimestampProto(time.Now())
    if err != nil {
        return fmt.Errorf("error building timestamp: %v", err)
    }

    ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
    defer cancel()

    BridgeNetworkSlice := make([]*bridge.Packet, n.MaxCaptureLength, n.MaxCaptureLength);

    for i, pkt := range n.NetworkSlice {
        BridgeNetworkSlice[i] = fbpacket.ToBridgePacket(pkt)
    }

    MSG := bridge.NetworkSlice{
        SubmissionNumber:0,
        TimeStamp: timestamp,
        Packets: BridgeNetworkSlice,         
        DevID: config.GetID(),
    }

    e := helpers.SaveToIntermediate(&MSG, "network")
    if e != nil {
        log.Errorf("error logging intermediate data to file: %v", e)
    }

    _, e = n.client.LogNetworkActivity(ctx, &MSG)
    if e != nil {
        return fmt.Errorf("error saving network slice: %v", e)
    }

    return nil
}

func (n *DataManager) AddToNetworkSlice(pkt fbpacket.Packet) error {

    n.SliceMutex.Lock()

    n.NetworkSlice[n.Index] = pkt;

    n.Index = (n.Index + 1) % n.MaxCaptureLength;
    if (n.Index != 0) { 
        n.SliceMutex.Unlock()
        return nil
    }

    err := n.SaveNetworkSlice(); 
    n.SliceMutex.Unlock()

    if err != nil {
        return fmt.Errorf("error saving network slice: %v", err)
    }

    log.Info("saved network slice")

    return nil
}

