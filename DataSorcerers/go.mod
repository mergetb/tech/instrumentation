module gitlab.com/instrumentation/DataSorcerers

go 1.22

toolchain go1.22.7

replace FusionBridge => ../FusionBridge

require (
	FusionBridge v0.0.0-00010101000000-000000000000
	github.com/fsnotify/fsnotify v1.8.0
	github.com/golang/protobuf v1.5.4
	github.com/google/gopacket v1.1.19
	github.com/shirou/gopsutil v2.21.11+incompatible
	github.com/sirupsen/logrus v1.9.3
	google.golang.org/grpc v1.68.1
	google.golang.org/protobuf v1.35.2
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/tklauser/go-sysconf v0.3.14 // indirect
	github.com/tklauser/numcpus v0.8.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.4 // indirect
	golang.org/x/net v0.29.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
	golang.org/x/text v0.18.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240903143218-8af14fe29dc1 // indirect
)
