package main;

import (
    "fmt"
    "time"
    "context"
    "io/ioutil"
    "google.golang.org/grpc" 
    "github.com/golang/protobuf/ptypes"

    bridge "FusionBridge/control/jupyter"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


var opts []grpc.DialOption;
var conn *grpc.ClientConn;
var client bridge.JupyterClient;

func ConnectToFusionCore() error {
    conn = helpers.CreateConnection()
    client = bridge.NewJupyterClient(conn); 

    return nil
}

func SaveData(full_path string) error {
    data, err := ioutil.ReadFile(full_path) 
    if err != nil {
        return fmt.Errorf("error reading file: %v: %v", full_path, err)
    }
 
    ctx, cancel := context.WithTimeout(context.Background(), 
        10*time.Second)
    defer cancel()
    
    timestamp, err := ptypes.TimestampProto(time.Now())
    if err != nil {
        return fmt.Errorf("error creating timestamp: %v", err)
    }

    MSG := bridge.IPYNB_Submission{
        SubmissionNumber : 0,
        TimeStamp : timestamp,
        FileLocation : full_path, 
        FileContents : string(data),
        DevID: config.GetID(),
    }

    e := helpers.SaveToIntermediate(&MSG, "jupyter")
    if e != nil {
        log.Errorf("error logging playbook to file: %v", e)
    }

    _, e = client.IngestIPYNB(ctx, &MSG)
    if e != nil {
        return fmt.Errorf("client error ingesting  ipynb: %v", e)
    }

    return nil
}


