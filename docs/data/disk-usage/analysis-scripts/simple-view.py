#!/bin/python3

import matplotlib.pyplot as plt


data = [ ]

# file = './data/docker/simple-pings/network-only.csv'
# file = './data/docker/simple-pings/file-only.csv'
# file = './data/docker/simple-pings/network-file.csv'
# file = './data/docker/baselines/network.csv'
# file = './data/docker/baselines/network-file.csv'
# file = './data/docker/baselines/file.csv'

# file = './data/mergetb/simple-pings/network-only.csv'
# file = './data/mergetb/simple-pings/file-only.csv'
# file = './data/mergetb/simple-pings/network-file.csv'
# file = './data/mergetb/baselines/network.csv'
# file = './data/mergetb/baselines/file.csv'
file = './data/mergetb/baselines/network-file.csv'


print(f"Reading {file}")


with open(file, 'r') as fd:
    for line in fd:
        line = line.strip()
        if line[-1] == ",":
            line = line[:-1]
        data += [int(line)]


# # Find slope of the data. Take in half second intervals, so .5x # of data points is the time
start = 50
stop = 250
slope = (data[stop] - data[start]) / ((stop - start) / 2)
print(f"Slope: {slope}")


# Create a line plot
plt.figure(figsize=(10, 6))
plt.plot(data, marker='o', linestyle='-', label='Data Points')
plt.title('Data Trend')
plt.xlabel('Index')
plt.ylabel('Value')
plt.grid(True)
plt.legend()
plt.show()



