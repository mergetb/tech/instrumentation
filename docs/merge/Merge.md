# DISCERN Integration with Merge Architecture

This document overviews how to integrate the DISCERN instrumentation system into Merge

## Architecture

![](./DISCERN-merge-integration.png)

The image above shows two potential architectures. 

### Ideal Setup 

The left side of the image shows an ideal setup that involves the following components:
- A dedicated storage server, which runs Postgres and InfluxDB databases
- An infrastructure server which hosts infrapods. Each infrapod runs the FusionCore service inside a container. The infrapod is confined to a single experiment, so each FusionCore instance is private to a single experiment.
- Hypervisors running virtual machines (VMs). Each VM runs the DataSorcerers. These communicate with the FusionCore service over the Merge infranet (the 172.30.0.0/16 network). 
- Bare metal machines operate the same way as VMs

### Initial Setup

The right side of the image shows an initial setup, which does not require any code changes in Merge or any operational updates to SPHERE/Mod-Deter (e.g., installing an OS on the storage server). This has the following components:
- The experiment model is updated so that a standalone management VM, which we call "overwatch", is added. The is a normal VM from the perspective of Merge. On this VM, we run FusionCore, Postgres, and InfluxDB. This is all done manually (with scripts) by the experimenter.
- The rest of the regular VMs and bare metal machines operate the same as in the Ideal Setup

The benefit of the initial setup is that we can start running experiments and analyzing data manually. Integration into Merge can be done without blocking this analysis.