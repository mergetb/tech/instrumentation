# The DataSorcerers' Configuration Options

This document is going to dry-ly list all the different configuration options you can specify. If 
you have architectural questions, please see `docs/architecture/DataSorcerers.md`


# Global Configuration Options

### All of these options need to match with the FusionCore server

**grpc_port:** An integer representing the port number of the Core server, 
    where all the services should send their data

    - default: 50051

**grpc_ip:** A string which holds the URL / IP for the Core server

    - default: localhost

**grpc_proto:** A string representing the protocol format used to send data to FusionCore

    - default: tcp

**maxrecvmsgsize:** An int used to set the MaxRecvMsgSize field in the gRPC connections

    - default : 10485760


## TLS Setings

**tls:** A boolean, used to determine if TLS is used in the connection
    
    - default: false

**certfile:** A string representing the location of the certification file 
    for the TLS connection

    - default: ""

**keyfile:** A string representing the location of the key file for the 
    TLS connection

    - default: ""


Every services logs its information to the same file, specified by the config parameter `logfile`


# Control Sorcerers Configuration


## Ansible Configuration

Sweeping for config files can be turned on and off using the option:
    
    saveansibleconfigs: true # default

Sweeping for playbooks can be controlled using:

    saveansibleplaybooks: true

The period of time between config sweeps is controlled with:

    ansibleconfigsweepinterval: 5 # default in seconds

The period of time between playbook sweeps is controlled with:

    ansibleplaybooksweepinterval: 5


## Bash Implementation And Configuration

The interval to dump the bash information to the FusionCore is controlled by:

    bashsweepinterval: 5 # default in seconds


# Log Sorcerer Configuration Parameters

**logsweepinterval:** An integer representing how often the logs should be
    recorded

    - default: 5

**loglinescaptured:** An integer representing how many lines should be 
    saved from the end of each file

    - default: 25

**logfiles:** An array of files which should be read and saved

    - default:
      - "/var/log/messages"
      - "/var/log/syslog"
      - "/var/log/auth.log"
      - "/var/log/daemon.log"
      - "/var/log/kern.log"



# Metadata Sorcerers Architecture

## File Metadata Sorcerer

The files which this programs listen to can be changed using the 
    config option: startingdirs. These directories are recursively 
    iterated down and listeners are added to all files and directories,

Certain folders can generate lots of data, so a blacklist is also 
    included to prevent the program from listening to certain files 
    and folders. This can be specified with: blacklistdirs.

**blacklistdirs:** An array of regexes which specify directories that shouldn't have file metadata 
saved

    - default: # these are regexes
        - '/home/\..*/\.config'
        - '/home/\..*/\..*'


**startingdirs:** An array of regexes which specify the directories to place file metadata listeners 
on. Directories will be recursively descended

    - default: "these are regexes
        - "/tmp"
        - "/home"


### Note

You may need to increase the following parameters, depending on how 
    many files you watch:

    fs.inotify.max_user_watches=124983
    fs.inotify.max_user_instances=128


## ID Metadata Sorcerer

The duration for scraping user information is controlled by:
    
    scrapforusers: 30000 # default in seconds

The duration for scraping interface inforamtion is controlled by:

    scrapeinterfaceinfo: 300 # default in seconds


## Network Metadata Sorcerer

These packets are created quite freqently and can quickly busy the 
    network. To get around this, you can adjust the parameter 
    networkslicelength. This parameter sets how many (packets, timestamp)
    pairs are saved before the data is sent to the Core Server

    networkslicelength: 25 # default in number of packets



