package main;


import (
    "fmt"
    "time"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


func main() {
    config.LoadConfig()
    if !config.Settings.RunCpu { return }

    err = log.InitializeLogOutput("CPU")
    if err != nil {
        fmt.Printf("error initializing file logger: %v", err)
        return
    }

    err = ConnectToFusionCore()
    if err != nil {
        log.Fatal("failed to connect to fusion core: %v", err)
    }

    if config.Settings.LogToIntermediateFile {
        err = helpers.BuildDir(config.Settings.IntermediateDataFolder)
        if err != nil {
            log.Fatalf("error building intermediate data dir: %v", err)
        }
    }



    for {
        // Save data about per cpu load usage
        percents, err := getCPULoad()
        if err != nil {
            log.Errorf("error getting cpu load info: %v", err)
        } else {
            err = SaveCPULoad(percents)
            if err != nil {
                log.Errorf("error saving cpu info to fusion core: %v", err)
            }
        }

        time.Sleep(5 * time.Second)
    }
}

