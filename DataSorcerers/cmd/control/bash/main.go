package main;

import (
    "os"
    "fmt"
    "time"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


func main() {
    config.LoadConfig()
    if !config.Settings.RunBash { return }

    err := log.InitializeLogOutput("Bash")
    if err != nil {
        fmt.Printf("error initializing logging: %v", err)
        return
    }

    err = ConnectToFusionCore()
    if err != nil {
        log.Fatalf("failed to connect to fusion core: %v", err)
    }

    if config.Settings.LogToIntermediateFile {
        err = helpers.BuildDir(config.Settings.IntermediateDataFolder)
        if err != nil {
            log.Fatalf("error building intermediate data dir: %v", err)
        }
    }

    for {
        filenames, err := getTTYLogFilenames()
        if err != nil {
            log.Errorf("error getting tty filenames: %v", err)
            continue // Could sleep here to avoid murdering the pc
        }
       
        for _, filename := range filenames {
            filePath := "/var/log/ttylog/" + filename

            // Format defined in start_ttylog
            // cnt is # of time they have sshd in so far, starting from 0
            host, user, cnt, err := parseUserDataFromFilename(filePath)
            if err != nil {
                log.Errorf("error parsing user data from %v: %v", filePath, err)
                continue
            }

            // Convert the ttylog to the csv file we'd like to export
            csvFilename, err := ttylogToCsv(filePath, host, user, cnt)
            if err != nil || len(csvFilename) == 0 {
                // Remove the problem file. Either its empty or we can't parse it
                if _, err := os.Stat(filePath); err == nil || !os.IsNotExist(err) {
                    // We don't care if these fail. If it does, files doesn't exists
                    _ = os.Remove(filePath) 
                    _ = os.Remove(csvFilename)
                }
                
                log.Errorf("error creating csv file for %v: %v", filePath, err)
                continue
            }

            // Send CSV Data to server
            err = saveCsvInCore(csvFilename, host, user, cnt)
            if err != nil { // Don't remove file so we can save it later?? Assuming this is core bug
                log.Errorf("Error saving %v core: %v", csvFilename, err)
                continue
            }

            log.Info("Sent message safely to the Fusion Core")

            // Remove the csv since its been dumped
            _ = os.Remove(csvFilename)

            // Empty the logs (as long as the data got properly sent & everything)
            err = emptyTTYLog(filePath)
            if err != nil {
                log.Errorf("Error emptying %v: %v", filePath, err)
                continue
            }
        }

        log.Info("finished sending logs. pausing...")
        time.Sleep(time.Duration(config.Settings.BashSweepInterval) * time.Second)
    }
}

