# System Architecture

![](./Architecture.svg)


## Documentation

This is effectively a quick start guide to installing and running the instrumentation. It couldn't possibly cover all the details required to understand the system. Luckily, `docs` contains all the information you want to know on the architecture, the configuration options, the build process, the testing environment, and the analysis scripts. 

If you need to know some information not listed here, its most likely detailed there. And in the unlikely event it isn't, create an issue and I'll update the documentation so it does!

To see everything that can be made, see `docs/make`, to get a detailed view of the architecture, see `docs/architecture`, to get a full list of the configuration options and what they mean, see `docs/conf`, and to see detailed information about the testing suite, see `docs/testing`

<br/>


## Installing onto a Merge Topology and Data Analysis

This repo is concerned exclusively with building the parts from source and their architecture. 
Most people that interact with this software don't need that level of detail. If you'd like to use 
the instrumentation software and do data analysis on it, please refer to the 
[Data Collection Repo](https://gitlab.com/BlankCanvasStudio/discern-collection#).

The Data Collection Repo contains scripts to install the sensors, build the fusioncore, run 
experiments, and exfiltrate data. All of this is useful, but entirely out of scope of this repo,
as this repo is meant purely for maintainers of the instrumentation.


## Installing From Source

To install everything, you can either install the individual debs provided in the package
registry, use the docker images provided in the image registry, or you can build from source.

The build dependencies and run-time dependencies are very different. So we wrote `deps.sh` to 
manage your dependencies.

To install the build dependencies, run:

```
./deps.sh build
```

To install FusionCore's runtime dependencies, run:

```
./deps.sh core
```

To install the Data Sorcerers' runtime dependencies, run:

```
./deps.sh sorcerers
```

To install all the dependencies at once, run:

```
./deps.sh all
```

To install the analysis scripts' dependencies, run:

```
./deps.sh analysis
```

The analysis script dependencies are **NOT** included in `./deps.sh all`

Make sure to run:

```
source ~/.profile
```

to add golang to your path. This won't be necessary after the first time.


And then you can simply run:

```
make all
``` 

from the project's root, and everything will build.

Once you've built everything, you can install and enable the DataSorcerers by running:

```
make install/DataSorcerers 
make enable
```

And you can install and run the FusionCore with:

```
make install/FusionCore
./FusionCore/build/FusionCore
```

But simply installing the debs will do all of this for you.


There is a testing build which can be run locally, without needing to use merge. To build it, run:

```
make testing
```

And start it with:

```
./Tests/sandbox-topology/run
```

This will stand up the FusionCore, the postgres database, the influx database, and an example 
node, set to record data. All of these containers can communicate out of the box and don't require any configuration. For more examples on the testing scenarios, see `docs/testing`.

To tear down the testing environment, run:

```
./Tests/sandbox-topology/run -t
```

Details on each of the components and how to build them are listed below.

<br/>
<br/>

# Fusion Bridge


This is the grpc protocol the Data Sorcerers use to communicate with the Fusion Core.

The bridge doesn't really "do" anything explicitly. It simply standardize the communication 
between the client and the server and allows us to automatically generate the server code in the 
FusionCore and the client protocols in the DataSorcerers.


### Dependencies

You can simply install all of the necessary dependencies with:

```
./deps.sh bridge
```

But if you must know, the build process depends on golang, unzip, protobuf-compiler, protoc, and grpc.

There is a version dependency on grpc and protoc, so they should be installed through go using the 
following commands:

```
go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
```


### Installation

If you are building the system from source, you **must** build this first. You can either build 
everything by running `make all` in the root directory, or you can build just the bridge by 
running:

```
make FusionBridge
```

from the root of the project.

<br/>
<br/>

# Data Sorcerers

These are a collection of static binaries which record and exfiltrate data on a single node over 
grpc (the Fusion Bridge) to the Fusion Core, which saves them to a database.


### Dependencies

The Sorcerers are controlled by systemd services, so you'll either need systemd or manually run 
all the programs (detailed below). 

The Sorcerers compile into static binaries, so you don't have any run time dependencies.

`./deps.sh build` will handle all the build dependencies for you.

But if you must know, the build process is dependent on libpcap-dev, gawk, and bison. 

The networking sorcerer compiles against `glibc 2.31`. If you need a different version of glibc, 
update line 5 of `deps.sh` to link against the correct version.




### Installation

You can install the Data Sorcerers using the `.deb` provided in the package registry, or you can 
build it from source.

To build the sensors from source, install the build dependencies (above) and run:

```
make DataSorcerers
```

To install the binaries, enable the systemd services, and enable the bpftrace programs, and start the Data Sorcerers, run:
 

```
make install/DataSorcerers
make enable
```

The Data Sorcerers are effectively just static binaries controlled by systemd. If you don't want to use systemd, you just need to run all the files that match the pattern `/usr/local/bin/discern-*-sorcerer`, `/usr/local/src/start_ttylog.sh`, and `DataSorcerers/dist/bpftrace/run_sensors.sh`. 

`/usr/local/src/start_ttylog.sh` controls bash logging and `DataSorcerers/dist/bpftrace/run_sensors.sh` controls the os metadata collection. If you don't need either of those, feel free to skip them. 

If you'd like more fine-grained control over the install process, please see `docs/make`

<br />
<br />

# Fusion Core

This is what ingests, sanitizes, and stores all the data from the data sorcerers into the postgres and 
influx databases.


### Dependencies

The FusionCore depends on `docker` to run its databases, so you need to install it first. 
Docker is automatically installed with `./deps.sh core`, `./deps.sh build` or `./deps.sh all`

If you are building from source, the Fusion Core build depends on the Fusion Bridge, so make sure 
you build the first (detailed above)


### Installation

You can install the FusionCore using the `.deb` provided in the package registry, or you can build 
it from source.

If you build from source make sure you install all the build dependencies first, build FusionBridge, and then run:

``` 
make FusionCore
```

in the root of the project.

To install and start the Fusion Core run:

```
make install
./FusionCore/build/FusionCore
```

This will copy `FusionCore/dist/conf/CoreConfig.yaml` to `/etc/discern/CoreConfig.yaml` for its 
run time settings

To manually start the influx pod or check if its running properly, run:

```
./FusionCore/build/influx
```

This reads `/etc/discern/CoreConfig.yaml` in your local file system and handles any influx 
operations for you.  By setting `startnewinfluxinstance: false`, this program becomes a way to 
check if the influx pod is currently active and can be communicated with. Just make sure 
`/etc/discern/CoreConfig.yaml` reflects your current setup.

Similarly, to check the postgres pod or check if its running properly, run:

```
./FusionCore/build/postgres
```
This reads `/etc/discern/CoreConfig.yaml` in your local file system and handles any postgres
operations for you.  By setting `startnewpostgresinstance: false`, this program becomes a way to 
check if the postgres pod is currently active and can be read from. Just make sure 
`/etc/discern/CoreConfig.yaml` reflects your current setup.


<br/>
<br/>


## Testing Environment

### Dependencies

These can all be installed with:

```
./deps.sh all
```

### Building

To build the sandbox testing environment, run:

```
make testing
```

### Running

To run an example docker deployment with a basic materialization pod run:

```
./Test/sandbox-topology/run
```

This will start an influx pod, a postgres pod, a fusioncore pod, and a basic materialization pod. 

These nodes will be able to communicate with eachother out of the box and have the following ip 
addresses in the local network:

- FusionCore: 10.10.10.2
    - Accessible via port 50051
- Postgres db: 10.10.10.3
    - Accessible via port 5432
- Influx db: 10.10.10.4
    - Accessible via port 8086
- Materialization node: 10.10.10.5
    - Accessible via docker cli

Your local machine will have the ip `10.10.10.1` in this network. 

To shut down the testing environment, run:

```
./Test/sandbox-topology/run -t
```

For more details, see `docs/testing`

