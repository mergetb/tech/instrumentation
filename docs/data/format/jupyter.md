# Saved Jupyter Data

FusionCore saves the notebook's data to the postgres database along side a hash of the content. 
This hash can be used as a foreign key to correlate with the records in the influx database.

You can find the postgres schema at: `FusionCore/dist/docker/postgres/schema/jupyter.sql`

FusionCore saves a hash of the data, with a device ID and the file's location to the influx 
database as a "jupyter" measurement.

`DevID` is a tag

`ContentHash` & `FileLocation` are fields

