package main;

import (
    "os"
    "fmt"
    "time"
    "bufio"
    "io/fs"
    "context"
    "strings"
    "io/ioutil"
    "google.golang.org/grpc" 
    "github.com/golang/protobuf/ptypes"

    bridge "FusionBridge/control/ansible"

    "gitlab.com/instrumentation/DataSorcerers/pkg/log"
    "gitlab.com/instrumentation/DataSorcerers/pkg/config"
    "gitlab.com/instrumentation/DataSorcerers/pkg/helpers"
)


var opts []grpc.DialOption;
var conn *grpc.ClientConn;
var client bridge.AnsibleClient;


func ConnectToFusionCore() error {
    conn = helpers.CreateConnection()
    client = bridge.NewAnsibleClient(conn); 

    return nil
}


func SaveConfig(path string) error {
    if path == "" { return nil }

    bindata, err := ioutil.ReadFile(path) 
    if err != nil { 
        if _, ok := err.(*fs.PathError); !ok {
            return fmt.Errorf("error reading config %v: %v", path, err)
        }
    }

    data := strings.TrimSpace(string(bindata))
    if data == "" { return nil }

    timestamp, err := ptypes.TimestampProto(time.Now())
    MSG := bridge.ConfigDetails{
        SubmissionNumber: 0,
        TimeStamp: timestamp,
        Location: path,
        Content: data,
        DevID: config.GetID(),
    }

    err = helpers.SaveToIntermediate(&MSG, "ansible-config")
    if err != nil {
        log.Errorf("error logging ansible-config to file: %v", err)
    }

    _, err = client.SaveAnsibleConfig(context.Background(), &MSG);
    if err != nil {
        return fmt.Errorf("error saving ansible config to core: %v", err)
    }

    return nil
}


func SaveYaml(path string) error {
    if path == "" { return nil }

    verified := VerifyPlaybook(path)

    ctx, cancel := context.WithTimeout(context.Background(), 
        10*time.Second)
    defer cancel()

    bindata, err := ioutil.ReadFile(path) 
    if err != nil { 
        if _, ok := err.(*fs.PathError); !ok {
            return fmt.Errorf("failed to read file %v: %v", path, err)
        }
    }

    data := strings.TrimSpace(string(bindata))
    if data == "" { return nil }

    timestamp, err := ptypes.TimestampProto(time.Now())

    MSG := bridge.PlaybookDetails{
        SubmissionNumber: 0,
        TimeStamp: timestamp,
        Location: path,
        Content: string(data),
        Verified: verified,
        DevID: config.GetID(),
    }

    err = helpers.SaveToIntermediate(&MSG, "ansible-yaml")
    if err != nil {
        log.Errorf("error logging ansible-config to file: %v", err)
    }

    _, err = client.SaveAnsiblePlaybook(ctx, &MSG);
    if err != nil {
        return fmt.Errorf("error saving yml %v: %v", path, err)
    }

    return nil
}


func VerifyPlaybook(path string) bool {
    file, err := os.Open(path)
    if err != nil { 
        log.Errorf("Error in opening file to verify playbook: %v", err) 
        return false
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        line := scanner.Text()

        if strings.Contains(line, "- hosts") {
            return true
        }
    }

    if err := scanner.Err(); err != nil {
        log.Errorf("Error in scanner while verifying playbook: %v", err)
    }

    return false
}

