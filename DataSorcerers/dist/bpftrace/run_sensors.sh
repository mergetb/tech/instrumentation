#!/bin/bash
sudo bpftrace -f json /etc/discern/bpfsensors/close.bt       >> /tmp/discern/data/os/close-res.txt       &
sudo bpftrace -f json /etc/discern/bpfsensors/closerange.bt >> /tmp/discern/data/os/closerange-res.txt &
sudo bpftrace -f json /etc/discern/bpfsensors/execve.bt      >> /tmp/discern/data/os/execve-res.txt      &
sudo bpftrace -f json /etc/discern/bpfsensors/execveat.bt    >> /tmp/discern/data/os/execveat-res.txt    &
sudo bpftrace -f json /etc/discern/bpfsensors/fork.bt        >> /tmp/discern/data/os/fork-res.txt        &
sudo bpftrace -f json /etc/discern/bpfsensors/kill.bt        >> /tmp/discern/data/os/kill-res.txt        &
sudo bpftrace -f json /etc/discern/bpfsensors/open.bt        >> /tmp/discern/data/os/open-res.txt        &
sudo bpftrace -f json /etc/discern/bpfsensors/openat.bt      >> /tmp/discern/data/os/openat-res.txt      &
sudo bpftrace -f json /etc/discern/bpfsensors/openat2.bt     >> /tmp/discern/data/os/openat2-res.txt     &
sudo bpftrace -f json /etc/discern/bpfsensors/recvfrom.bt    >> /tmp/discern/data/os/recvfrom-res.txt    &
sudo bpftrace -f json /etc/discern/bpfsensors/recvmmsg.bt    >> /tmp/discern/data/os/recvmmsg-res.txt    &
sudo bpftrace -f json /etc/discern/bpfsensors/recvmsg.bt     >> /tmp/discern/data/os/recvmsg-res.txt     &
sudo bpftrace -f json /etc/discern/bpfsensors/sendmmsg.bt    >> /tmp/discern/data/os/sendmmsg-res.txt    &
sudo bpftrace -f json /etc/discern/bpfsensors/sendmsg.bt     >> /tmp/discern/data/os/sendmsg-res.txt     &
sudo bpftrace -f json /etc/discern/bpfsensors/sendto.bt      >> /tmp/discern/data/os/sendto-res.txt      &
sudo bpftrace -f json /etc/discern/bpfsensors/socket.bt      >> /tmp/discern/data/os/socket-res.txt      &
sudo bpftrace -f json /etc/discern/bpfsensors/socketpair.bt  >> /tmp/discern/data/os/socketpair-res.txt  &
sudo bpftrace -f json /etc/discern/bpfsensors/sysinfo.bt     >> /tmp/discern/data/os/sysinfo-res.txt     &
sudo bpftrace -f json /etc/discern/bpfsensors/tkill.bt       >> /tmp/discern/data/os/tkill-res.txt       &
sudo bpftrace -f json /etc/discern/bpfsensors/vfork.bt       >> /tmp/discern/data/os/vfork-res.txt       &

# Errors exist in these for some reason
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/execve.bt      >> /tmp/discern/data/os/execve-res.txt      "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/execveat.bt    >> /tmp/discern/data/os/execveat-res.txt    "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/kill.bt        >> /tmp/discern/data/os/kill-res.txt        "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/open.bt        >> /tmp/discern/data/os/open-res.txt        "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/openat.bt      >> /tmp/discern/data/os/openat-res.txt      "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/openat2.bt     >> /tmp/discern/data/os/openat2-res.txt     "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/recvfrom.bt    >> /tmp/discern/data/os/recvfrom-res.txt    "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/recvmmsg.bt    >> /tmp/discern/data/os/recvmmsg-res.txt    "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/recvmsg.bt     >> /tmp/discern/data/os/recvmsg-res.txt     "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/sendmmsg.bt    >> /tmp/discern/data/os/sendmmsg-res.txt    "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/sendmsg.bt     >> /tmp/discern/data/os/sendmsg-res.txt     "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/sendto.bt      >> /tmp/discern/data/os/sendto-res.txt      "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/socket.bt      >> /tmp/discern/data/os/socket-res.txt      "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/socketpair.bt  >> /tmp/discern/data/os/socketpair-res.txt  "
# sudo bash -c "bpftrace -f json /etc/discern/bpfsensors/tkill.bt       >> /tmp/discern/data/os/tkill-res.txt       "
