# Sandbox Test

This test builds a small docker network on your local machine, starts an influx container, a 
postgres container, a FusionCore container, and a example materialization container to record data
on. 

This test suite automatically connects all of them and is configured so that they can 
communicate out of the box.

<br />

## Building the Sandbox Test Suite

**NOTE:** Don't try to rebuild the images while the Sandbox test suite is running. Docker can be a
little bit wonky about creating new versions of running images.

To build the test environment, in the root of the project, run:

```
make testing
```

This builds the standard postgres image, the standard influx image, and the FusionCore image with 
`FusionCore/dist/conf/SandboxConf.yaml` as its `/etc/discern/CoreConfig.yaml`

If you'd like to rebuild the FusionCore Sandbox image, run:

```
cd FusionCore
make docker/Sandbox
```

If you need to rebuild the database images run:

```
cd FusionCore
make docker/postgres
make docker/influx
```

<br />

## Running the Sandbox Test Suite

To start the test environment, run:

```
./Tests/sandbox-topology/run
```

<br />

## Stopping the Sandbox Test Suite

To stop the test environment, run:

```
./Tests/sandbox-topology/run -t
```

<br />

## Implementation Details


The test suite will create a bridge on your local machine and will allocate **you** the ip 
`10.10.10.1`. It will set the FusionCore's IP to `10.10.10.2`, it will set the postgres 
container's ip to `10.10.10.3`, it will set the influx container's ip to `10.10.10.4`, and it will 
set the example materialization pods ip to `10.10.10.5`.

You can ping any of the machine with `ping 10.10.10.X` and it will originate from the ip 
`10.10.10.1`, your local address in the network.

The docker **images** are named: sandbox, discernpsql, discerninflux, and discernpod. The are **running with the names:** fusioncore, discernpsql, discerninflux, and discernpod, respectively. You can guess what does what. 

If you need to access a cli in any of the pods, run:

```
docker exec -it <pod name> /bin/bash
```

For example, if I need a shell to execute commands in the fusioncore container (running the sandbox image), I'd run:

```
docker exec -it fusioncore /bin/bash
```

