from mergexp import *

net = Network("two-emu-test", routing == static, addressing == ipv4)
a   = net.node('a', metal==False, image == "bullseye", proc.cores >= 4, memory.capacity>=gb(8))
b   = net.node('b', metal==False, image == "bullseye")
c   = net.node('c', metal==False, image == "bullseye")
d   = net.node('d', metal==False, image == "bullseye")
e   = net.node('e', metal==False, image == "bullseye")


link = net.connect([a,b, c, d, e])
link[a].socket.addrs = ip4("10.0.6.1/24")
link[b].socket.addrs = ip4("10.0.6.2/24")
link[c].socket.addrs = ip4("10.0.6.3/24")
link[d].socket.addrs = ip4("10.0.6.4/24")
link[e].socket.addrs = ip4("10.0.6.5/24")


experiment(net)
